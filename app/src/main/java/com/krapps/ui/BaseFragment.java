package com.krapps.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;

import com.krapps.listener.UpdateJsonListener;
import com.krapps.listener.UpdateListener.onUpdateViewListener;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;

import java.util.HashMap;


public abstract class BaseFragment extends Fragment implements OnClickListener,
        UpdateJsonListener.onUpdateViewListener {
    public static String HOME_FRAGMENT = "frg_home";
    private String frgamentName;
    public BaseActivity mActivity;
    private float density;
    private String agreement_id;

    public BaseFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        density = mActivity.getResources().getDisplayMetrics().density;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
        }
    }


    /*public void replaceChildFragment(Fragment fragment, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragmentLocal = getChildFragmentManager().findFragmentById(
                R.id.innerFrame);
        if (fragmentLocal != null
                && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        ft.replace(R.id.innerFrame, fragment, tag);

        // fragment.setRetainInstance(true);
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
        ((BaseActivity) mActivity).removeProgressDialog();
    }

    public HashMap<String, String> getBaseParams() {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }

    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }

    public float getDensity() {
        return density;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (BaseActivity) activity;
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragmentName(String frgamentName) {
        this.frgamentName = frgamentName;
    }

    public String getFragmentName() {
        return frgamentName;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(),
                    getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = getBaseParams();


        return params;
    }

    /**
     * Override this method when refresh page is required in your fragment
     *
     * @param bundle
     */
    protected void refreshFragment(Bundle bundle) {

    }

    public void updateView(Object object) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                break;
        }

        Log.d("click", BaseFragment.class.getSimpleName().toString());

    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(),
                        getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {

                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
