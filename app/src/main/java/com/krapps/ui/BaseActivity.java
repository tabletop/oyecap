package com.krapps.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.picasso.BitmapBorderTransformation;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.HashMap;


//import com.bugsense.trace.BugSenseHandler;

public class BaseActivity extends AppCompatActivity implements UpdateJsonListener.onUpdateViewListener, OnClickListener {

    private static AlertDialog alertDialog;
    private BaseApplication mApplication;
    private ProgressDialog mProgressDialog;
    private String mDeviceId;
    private Picasso picasso;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        picasso = Picasso.with(this);
        mApplication = (BaseApplication) getApplication();

    }

    public void clearAllFragment() {
        FragmentManager fm = this.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }


    protected void setupCancelToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.close);
        if (!StringUtils.isNullOrEmpty(title)) {
            getSupportActionBar().setTitle(title);
        }
    }


    protected void setupBackToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (title != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    /*public void replaceFragment(String currentFragmentTag, Fragment fragment, Bundle bundle, boolean isAddToBackStack) {
        findViewById(R.id.content_frame).setVisibility(View.VISIBLE);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentLocal = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragmentLocal != null && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        if (!StringUtils.isNullOrEmpty(currentFragmentTag)) {
            ft.add(R.id.content_frame, fragment, tag);
            Fragment fragmentToHide = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragmentToHide != null) {
                ft.hide(fragmentToHide);
            }
        } else {
            ft.replace(R.id.content_frame, fragment, tag);
        }

        fragment.setRetainInstance(true);
        if (isAddToBackStack) {
            ft.addToBackStack(tag);
        }
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }*/


    public void showProgressDialog() {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage("Loading");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mApplication.activityResumed();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mApplication.activityPaused();
    }

    /**
     * hides the soft key pad
     */
    public void hideSoftKeypad(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(BaseActivity.class.getSimpleName(), "hideSoftKeypad()", e);
        }
    }

    public void loadUrlOnBrowser(String url) {
        try {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception ex) {
            ToastUtils.showToast(this, "Url is not proper!");
        }
    }

    @Override
    public void onClick(View view) {

    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                // ToastUtils.showToast(this,
                // getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {

                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void hitApiRequest(int reqType) throws JSONException {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {


            default:
                url = "";
                className = null;
                break;
        }

        VolleyJsonRequest request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
        }, getJsonString(reqType));
        ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
    }

    protected String getJsonString(int reqType) {


        return null;
    }

    public void onStop() {
        super.onStop();


    }

    public enum HEADER_MODE {
        MENU_ONLY, MENU_WITH_SEARCH, BACK_WITH_SEARCH, BACK_WITHOUT_SEARCH, BACK_WITH_FORWARD, ONLY_FORWARD, MENU_WITH_SETTING, BOTH_SIDE_TEXT, SEARCH;
    }

    public void loadParseFileInBackground(String url, final ImageView imgView, int placeHolder) {
        if (!StringUtils.isNullOrEmpty(url)) {
            if (placeHolder != -1) {
                picasso.load(ApiConstants.URL_IMAGE + url).placeholder(placeHolder).centerCrop().fit().into(imgView);
            } else {
                picasso.load(ApiConstants.URL_IMAGE + url).centerCrop().fit().into(imgView);
            }
        }
    }


    public void loadRoundedCornerImage(String url, final ImageView imgView) {
        Picasso picasso = Picasso.with(this);
        if (!StringUtils.isNullOrEmpty(url) && imgView != null) {
            picasso.load(ApiConstants.URL_IMAGE + url)
                    .transform(new BitmapBorderTransformation(0, 25, Color.TRANSPARENT))
                    .centerCrop()
                    .fit()
                    .into(imgView);
        }
    }
}
