package com.krapps.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.krapps.database.BaseSqliteOpenHelper;
import com.krapps.network.VolleyManager;

/**
 * @author kapil.vij
 */
public class BaseApplication extends Application {
    private static BaseSqliteOpenHelper dbHelper;
    public static Context mContext;

    private boolean activityVisible;
    private String mFriendChatUserName;
    private boolean activityChatVisible;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public boolean isActivityVisible() {
        return activityVisible;
    }

    public void activityResumed() {
        activityVisible = true;
    }

    public void activityPaused() {
        activityVisible = false;
    }

    public boolean isActivityChatVisible() {
        return activityChatVisible;
    }

    public void activityChatResumed(String pFriendChatUserName) {
        activityChatVisible = true;
        mFriendChatUserName = pFriendChatUserName;
    }

    public String getFocusedChatUser() {
        return mFriendChatUserName;
    }

    public void activityChatPaused() {
        activityChatVisible = false;
    }

    public VolleyManager getVolleyManagerInstance() {
        return VolleyManager.getInstance(getApplicationContext());
    }

    public static BaseSqliteOpenHelper getDbHelperInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new BaseSqliteOpenHelper(context);
        }
        return dbHelper;
    }

    @Override
    public void onTerminate() {
        dbHelper.close();
        super.onTerminate();
    }

}
