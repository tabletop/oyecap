package com.krapps.network;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.listener.UpdateListener;
import com.oyecaptain.BuildConfig;
import com.oyecaptain.utils.OyeCapPrefernece;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * @author kapil.vij
 */
public class VolleyJsonRequest extends JsonObjectRequest {

    private VolleyJsonRequest(int method, String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {

        super(method, url, requestJson == null ? null : new JSONObject(requestJson), updateListener, updateListener);
        /*if (requestJson == null) {
            super(url, null, updateListener, updateListener);
        } else {
            super(url, null, updateListener, updateListener);
        }*/

    }

    public static VolleyJsonRequest doPost(String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
            Log.i("Request Jso-->", requestJson);
        }
        return new VolleyJsonRequest(Method.POST, url, updateListener, requestJson);
    }

    public static VolleyJsonRequest doget(String url, UpdateJsonListener updateListener) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
        }
        return new VolleyJsonRequest(Method.GET, url, updateListener, null);
    }

    public static VolleyJsonRequest dodelete(String url, UpdateJsonListener updateListener, String json) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
        }
        return new VolleyJsonRequest(Method.DELETE, url, updateListener, json);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        super.getHeaders();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("authorization", "Bearer " + OyeCapPrefernece.getInstance().getToken());
        return hashMap;
    }
}