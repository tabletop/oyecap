package com.oyecaptain.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.krapps.application.BaseApplication;
import com.oyecaptain.constants.AppConstants;

public class OyeCapPrefernece {

    private static SharedPreferences mPreferences;
    private static OyeCapPrefernece mInstance;
    private static Editor mEditor;

    private static String IS_LOGGED_IN = "is_logged_in";
    private static String USER_ID = "user_id";
    private static String TOKEN = "token";
    private static String NAME = "name";
    private static String EMAIL = "email";


    private OyeCapPrefernece() {
    }

    public static OyeCapPrefernece getInstance() {
        if (mInstance == null) {
            Context context = BaseApplication.mContext;
            mInstance = new OyeCapPrefernece();
            mPreferences = context.getSharedPreferences(AppConstants.OYE_CAP_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }

    public void setLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN, value).apply();
    }

    public boolean getLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN, false);
    }


    public String getUserId() {
        return mPreferences.getString(USER_ID, "");
    }

    public void setUserId(String value) {
        mEditor.putString(USER_ID, value).apply();
    }

    public String getToken() {
        return mPreferences.getString(TOKEN, "");
    }

    public void setToken(String token) {
        mEditor.putString(TOKEN, token).apply();
    }


    public String getName() {
        return mPreferences.getString(NAME, "");
    }

    public void setName(String token) {
        mEditor.putString(NAME, token).apply();
    }


    public String getEmail() {
        return mPreferences.getString(EMAIL, "");
    }

    public void setEmail(String token) {
        mEditor.putString(EMAIL, token).apply();
    }

    public void clear() {
        mEditor.clear().apply();
    }
}