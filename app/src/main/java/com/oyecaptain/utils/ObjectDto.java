package com.oyecaptain.utils;

import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MegaLeagues;

/**
 * Created by monish on 18/11/16.
 */

public class ObjectDto {
    public static Matches getMatchFromMegaLeague(MegaLeagues megaLeagues) {
        Matches matches = new Matches();
        matches.setName(megaLeagues.getMatch_name());
        matches.setFormat_time(megaLeagues.getFormat_time());
        matches.setTeams(megaLeagues.getTeams());
        matches.setId(megaLeagues.getMatch_id());
        matches.setTournament(megaLeagues.getTournament_id());
        return matches;
    }
}
