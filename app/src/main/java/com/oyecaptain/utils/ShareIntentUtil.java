package com.oyecaptain.utils;

import android.content.Context;
import android.content.Intent;


public class ShareIntentUtil {


    public static void shareEvent(Context context, String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String type = "text/plain";

        sendIntent.setType(type);

        sendIntent.putExtra(Intent.EXTRA_TEXT, url);

        context.startActivity(Intent.createChooser(sendIntent, "Share OyeCap"));
    }

}
