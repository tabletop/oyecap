package com.oyecaptain.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.response.JoinedLeagueResponse;
import com.oyecaptain.ui.adapter.PaymentMethodPagerAdapter;
import com.payUMoney.sdk.PayUmoneySdkInitilizer;
import com.payUMoney.sdk.SdkConstants;

import org.json.JSONException;

/**
 * Created by daemonn on 20/05/16.
 */
public class PaymentMethodActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    String TAG = PaymentMethodActivity.class.getSimpleName();

    private TabLayout slidingTabLayout;
    private ViewPager viewPager;
    private String url;
    private PaymentMethodPagerAdapter paymentMethodPagerAdapter;
    public String matchId;
    public String matchname;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        setupBackToolBar("SELECT PAYMENT METHOD");

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        paymentMethodPagerAdapter = new PaymentMethodPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(paymentMethodPagerAdapter);


        setUpSlidingTabLayout();

    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_JOINED_LEAGUES:
                    url = String.format(ApiConstants.URL_JOINED_LEAGUES, matchId);
                    className = JoinedLeagueResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(this, this, reqType, className) {
                    });
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_JOINED_LEAGUES) {
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_JOINED_LEAGUES:
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                super.onClick(view);

        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setUpSlidingTabLayout() {
        slidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        // slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setBackgroundResource(R.color.colorPrimary);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayUmoneySdkInitilizer.PAYU_SDK_PAYMENT_REQUEST_CODE) {


            if (resultCode == RESULT_OK) {
                Log.i(TAG, "Success - Payment ID : " + data.getStringExtra(SdkConstants.PAYMENT_ID));
                String paymentId = data.getStringExtra(SdkConstants.PAYMENT_ID);
                ToastUtils.showToast(this, "Payment Success Id : " + paymentId);
            } else if (resultCode == RESULT_CANCELED) {
                Log.i(TAG, "failure");
                ToastUtils.showToast(this, "cancelled");
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_FAILED) {
                Log.i("app_activity", "failure");

                if (data != null) {
                    if (data.getStringExtra(SdkConstants.RESULT).equals("cancel")) {

                    } else {
                        ToastUtils.showToast(this, "failure");
                    }
                }
                //Write your code if there's no result
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_BACK) {
                Log.i(TAG, "User returned without login");
                ToastUtils.showToast(this, "User returned without login");
            }
        }
    }
}
