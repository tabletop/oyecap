package com.oyecaptain.ui.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.request.NewPasswordRequest;
import com.oyecaptain.model.response.ForgotPasswordResponse;
import com.oyecaptain.utils.MyDatePickerDialog;

import org.json.JSONException;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by daemonn on 20/05/16.
 */
public class ChangePasswordActivity extends BaseActivity {

    String TAG = ChangePasswordActivity.class.getSimpleName();

    EditText edtOtp;
    EditText edtChangePass;
    EditText edtChangeConPass;
    private String email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Intent intent = getIntent();
        if (intent != null) {
            email = intent.getStringExtra(AppConstants.EXTRA_EMAIL);
        }

        edtOtp = (EditText) findViewById(R.id.edtOtp);
        edtChangePass = (EditText) findViewById(R.id.edtChangePass);
        edtChangeConPass = (EditText) findViewById(R.id.edtChangeConPass);

        findViewById(R.id.edtChangeConfirm).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtChangeConfirm:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_NEW_PASSWORD, true);
                }
                break;
            default:
                super.onClick(view);

        }
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            String url;
            switch (reqType) {
                case ApiConstants.REQUEST_NEW_PASSWORD:
                    url = ApiConstants.URL_NEW_PASSWORD;
                    className = ForgotPasswordResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_NEW_PASSWORD) {
            NewPasswordRequest newPasswordRequest = new NewPasswordRequest();
            newPasswordRequest.setEmail(email);
            newPasswordRequest.setOtp(edtOtp.getText().toString().trim());
            newPasswordRequest.setPassword(edtChangeConPass.getText().toString().trim());
            return new Gson().toJson(newPasswordRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_NEW_PASSWORD:
                    ForgotPasswordResponse forgotPasswordResponse = (ForgotPasswordResponse) responseObject;
                    if (forgotPasswordResponse.isSuccess()) {
                        AlertDialogUtils.showAlertDialog(this, "OyeCap", "Password successfully changed", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    } else {
                        ToastUtils.showToast(this, "Password not chnaged");
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtOtp.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter Otp");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtChangePass.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtChangeConPass.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter confirm password");
            return false;
        } else if (!edtChangePass.getText().toString().trim().equals(edtChangeConPass.getText().toString().trim())) {
            ToastUtils.showToast(this, "password and confirm password do not match");
            return false;
        }
        return true;
    }


}
