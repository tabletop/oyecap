package com.oyecaptain.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.EmailLoginRequest;
import com.oyecaptain.model.response.LoginResponse;
import com.oyecaptain.utils.OyeCapPrefernece;

import org.json.JSONException;

/**
 * Created by daemonn on 20/05/16.
 */
public class LoginActivity extends BaseActivity {

    String TAG = LoginActivity.class.getSimpleName();
    private EditText edtPassword;
    private EditText edtEmail;
    private String url;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupCancelToolBar("LOGIN");

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        findViewById(R.id.txtLogin).setOnClickListener(this);
        findViewById(R.id.txtforgotPass).setOnClickListener(this);


    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN_EMAIL:
                    url = ApiConstants.URL_LOGIN;
                    className = LoginResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_LOGIN_EMAIL) {
            EmailLoginRequest emailLoginRequest = new EmailLoginRequest();
            emailLoginRequest.setEmail(edtEmail.getText().toString().trim());
            emailLoginRequest.setPassword(edtPassword.getText().toString().trim());
            return new Gson().toJson(emailLoginRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN_EMAIL:
                    LoginResponse loginResponse = (LoginResponse) responseObject;
                    OyeCapPrefernece.getInstance().setLoggedIn(true);
                    OyeCapPrefernece.getInstance().setUserId(loginResponse.getUser_id());
                    OyeCapPrefernece.getInstance().setToken(loginResponse.getToken());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, 500);
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtLogin:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_LOGIN_EMAIL, true);
                }
                break;
            case R.id.txtforgotPass:
                intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;

            default:
                super.onClick(view);

        }
    }

    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtEmail.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter email");
            return false;
        } else if (!StringUtils.isValidEmail(edtEmail.getText().toString().trim(), false)) {
            ToastUtils.showToast(this, "Please enter a valid email");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter password");
            return false;
        }
        return true;
    }


}
