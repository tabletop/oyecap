package com.oyecaptain.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.request.EmailLoginRequest;
import com.oyecaptain.model.request.ForgotPasswordRequest;
import com.oyecaptain.model.response.ForgotPasswordResponse;
import com.oyecaptain.model.response.LoginResponse;
import com.oyecaptain.utils.OyeCapPrefernece;

import org.json.JSONException;

/**
 * Created by daemonn on 20/05/16.
 */
public class ForgotPasswordActivity extends BaseActivity {

    String TAG = ForgotPasswordActivity.class.getSimpleName();
    EditText forgot_email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        setupCancelToolBar("FORGOT PASSWORD");
        forgot_email = (EditText) findViewById(R.id.forgot_email);

        findViewById(R.id.forgot_confirm).setOnClickListener(this);

    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            String url;
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    url = ApiConstants.URL_FORGOT_PASSWORD;
                    className = ForgotPasswordResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_FORGOT_PASSWORD) {
            ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
            forgotPasswordRequest.setEmail(forgot_email.getText().toString().trim());
            return new Gson().toJson(forgotPasswordRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    ForgotPasswordResponse forgotPasswordResponse = (ForgotPasswordResponse) responseObject;
                    if (forgotPasswordResponse.isSuccess()) {
                        AlertDialogUtils.showAlertDialog(this, "OyeCap", "Mail has been send to your register Email", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(ForgotPasswordActivity.this, ChangePasswordActivity.class);
                                intent.putExtra(AppConstants.EXTRA_EMAIL, forgot_email.getText().toString().trim());
                                startActivity(intent);
                                finish();
                            }
                        });
                    } else {
                        ToastUtils.showToast(this, "Mail has not been send");
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_confirm:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_FORGOT_PASSWORD, true);
                }
                break;
            default:
                super.onClick(view);

        }
    }


    private boolean validate() {
        if (StringUtils.isNullOrEmpty(forgot_email.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter email");
            return false;
        } else if (!StringUtils.isValidEmail(forgot_email.getText().toString().trim(), false)) {
            ToastUtils.showToast(this, "Please enter valid email");
            return false;
        }
        return true;
    }

}
