package com.oyecaptain.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.model.CommonJsonResponse;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.request.SaveTeamRequest;
import com.oyecaptain.model.response.ForgotPasswordResponse;
import com.oyecaptain.model.response.Players;
import com.oyecaptain.ui.adapter.CaptainRecyclerAdapter;
import com.oyecaptain.ui.adapter.ViceCaptainRecyclerAdapter;
import com.oyecaptain.utils.VerticalItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daemonn on 20/05/16.
 */
public class CaptainViceCaptainActivity extends BaseActivity {

    String TAG = CaptainViceCaptainActivity.class.getSimpleName();
    private String url;
    private RecyclerView listviewCaptain;
    private RecyclerView listviewViceCaptain;
    private CaptainRecyclerAdapter captainRecyclerAdapter;
    private ViceCaptainRecyclerAdapter viceCaptainRecyclerAdapter;
    private ArrayList<Players> playersList;
    private int captainId;
    private int viceCaptainId;
    private String matchId;
    private String tournamentId;
    private String matchName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_captain);

        setupBackToolBar("SELECT CAPTAIN");

        Intent intent = getIntent();
        if (intent != null) {
            playersList = intent.getParcelableArrayListExtra(AppConstants.EXTRA_PLAYERS);
            matchId = intent.getStringExtra(AppConstants.EXTRA_MATCH_ID);
            tournamentId = intent.getStringExtra(AppConstants.EXTRA_TOURNAMENT_ID);
            matchName = intent.getStringExtra(AppConstants.EXTRA_MATCH_NAME);
        }


        listviewCaptain = (RecyclerView) findViewById(R.id.listviewCaptain);
        listviewCaptain.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listviewCaptain.setLayoutManager(linearLayoutManager);
        captainRecyclerAdapter = new CaptainRecyclerAdapter(this, this);
        listviewCaptain.setAdapter(captainRecyclerAdapter);


        listviewViceCaptain = (RecyclerView) findViewById(R.id.listviewViceCaptain);
        listviewViceCaptain.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this);
        listviewViceCaptain.setLayoutManager(linearLayoutManager1);
        viceCaptainRecyclerAdapter = new ViceCaptainRecyclerAdapter(this, this);
        listviewViceCaptain.setAdapter(viceCaptainRecyclerAdapter);

        setCaptainnAdapter(playersList);
        setViceCaptainAdapter(playersList);

        findViewById(R.id.txtSave).setOnClickListener(this);

    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_SAVE_TEAM:
                    url = ApiConstants.URL_SAVE_TEAM;
                    className = CommonJsonResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_SAVE_TEAM) {
            SaveTeamRequest saveTeamRequest = new SaveTeamRequest();
            saveTeamRequest.setC_id(captainId + "");
            saveTeamRequest.setVc_id(viceCaptainId + "");
            saveTeamRequest.setMatch_id(matchId);
            saveTeamRequest.setTournament_id(tournamentId);
            List<String> players = new ArrayList<>();
            for (Players temp : playersList) {
                players.add(temp.getId() + "");
            }
            saveTeamRequest.setPlayers(players);
            return new Gson().toJson(saveTeamRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_SAVE_TEAM:
                    CommonJsonResponse commonJsonResponse = (CommonJsonResponse) responseObject;
                    if (commonJsonResponse.isSuccess()) {
                        AlertDialogUtils.showAlertDialog(this, "Oye captain", "Team Successfully created", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(CaptainViceCaptainActivity.this, LeagueSelectionActivity.class);
                                intent.putExtra(AppConstants.EXTRA_MATCH_ID, matchId + "");
                                intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matchName);
                                startActivity(intent);
                            }
                        });
                    } else {
                    }
                    break;

                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void setViceCaptainAdapter(ArrayList<Players> playersList) {
        viceCaptainRecyclerAdapter.setListData(playersList);
        viceCaptainRecyclerAdapter.notifyDataSetChanged();
    }


    private void setCaptainnAdapter(ArrayList<Players> playersList) {
        captainRecyclerAdapter.setListData(playersList);
        captainRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rtlCaptainParent:
                Players players = (Players) view.getTag();
                markAllCaptainUnSelected();
                if (captainRecyclerAdapter.getListdata().contains(players)) {
                    captainId = players.getId();
                    if (captainId == viceCaptainId) {
                        ToastUtils.showToast(this, "Captain and Vice Captain cannot be same");
                        return;
                    }
                    int pos = captainRecyclerAdapter.getListdata().indexOf(players);
                    captainRecyclerAdapter.getListdata().get(pos).setCaptain(true);
                    captainRecyclerAdapter.notifyDataSetChanged();
                }
                break;
            case R.id.rtlViceCaptainParent:
                Players players1 = (Players) view.getTag();
                markAllViceCaptainUnSelected();
                if (viceCaptainRecyclerAdapter.getListdata().contains(players1)) {
                    viceCaptainId = players1.getId();
                    if (captainId == viceCaptainId) {
                        ToastUtils.showToast(this, "Captain and Vice Captain cannot be same");
                        return;
                    }
                    int pos = viceCaptainRecyclerAdapter.getListdata().indexOf(players1);
                    viceCaptainRecyclerAdapter.getListdata().get(pos).setViceCaptain(true);
                    viceCaptainRecyclerAdapter.notifyDataSetChanged();
                }
                break;
            case R.id.txtSave:
                if (viceCaptainId == 0) {
                    ToastUtils.showToast(this, "Please select Vice Captain");
                } else if (captainId == 0) {
                    ToastUtils.showToast(this, "Please select Captain");
                } else {
                    hitApiRequest(ApiConstants.REQUEST_SAVE_TEAM, true);
                }
                break;
            default:
                super.onClick(view);

        }
    }

    private void markAllViceCaptainUnSelected() {
        for (Players temp : viceCaptainRecyclerAdapter.getListdata()) {
            temp.setViceCaptain(false);
        }
    }

    private void markAllCaptainUnSelected() {
        for (Players temp : captainRecyclerAdapter.getListdata()) {
            temp.setCaptain(false);
        }
    }


}
