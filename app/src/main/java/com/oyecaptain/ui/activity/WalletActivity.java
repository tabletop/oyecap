package com.oyecaptain.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.krapps.ui.BaseActivity;
import com.oyecaptain.R;
import com.oyecaptain.ui.adapter.HomePagerAdapter;
import com.oyecaptain.ui.adapter.MyGamesAdapter;
import com.oyecaptain.ui.adapter.WalletPagerAdapter;
import com.oyecaptain.ui.fragment.MyContestFragment;
import com.oyecaptain.utils.CustomViewPager;

/**
 * Created by daemonn on 20/05/16.
 */
public class WalletActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    String TAG = WalletActivity.class.getSimpleName();

    private TabLayout slidingTabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private String url;
    private WalletPagerAdapter walletPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        setupBackToolBar("WALLET");

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        walletPagerAdapter = new WalletPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(walletPagerAdapter);

        setUpSlidingTabLayout();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                super.onClick(view);

        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setUpSlidingTabLayout() {
        slidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        // slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setBackgroundResource(R.color.colorPrimary);
    }
}
