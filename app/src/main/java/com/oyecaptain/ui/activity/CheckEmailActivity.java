package com.oyecaptain.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.ForgotPasswordRequest;
import com.oyecaptain.model.response.ForgotPasswordResponse;

import org.json.JSONException;

/**
 * Created by K.Agg on 12-11-2016.
 */

public class CheckEmailActivity extends BaseActivity {

    String TAG = ChangePasswordActivity.class.getSimpleName();
EditText edtEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_email);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        findViewById(R.id.txtConfirm).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_confirm:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_LOGIN_EMAIL, true);
                }

                break;
            default:
                super.onClick(view);

        }
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            String url;
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    url = ApiConstants.URL_FORGOT_PASSWORD;
                    className = ForgotPasswordResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_FORGOT_PASSWORD) {
           // return new Gson().toJson(forgotPasswordRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    ForgotPasswordResponse forgotPasswordResponse = (ForgotPasswordResponse) responseObject;
                    if (forgotPasswordResponse.isSuccess()) {
                        ToastUtils.showToast(this, "Mail has been send to your register Email");
                        finish();
                    } else {
                        ToastUtils.showToast(this, "Mail has not been send");
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }



    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtEmail.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter Email");
            return false;
        }
        return true;
    }




}
