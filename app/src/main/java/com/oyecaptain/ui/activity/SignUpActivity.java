package com.oyecaptain.ui.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.EmailLoginRequest;
import com.oyecaptain.model.request.RegisterRequest;
import com.oyecaptain.model.response.BalanceHistoryResponse;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.RegisterResponse;
import com.oyecaptain.utils.MyDatePickerDialog;
import com.oyecaptain.utils.OyeCapPrefernece;

import org.json.JSONException;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by daemonn on 20/05/16.
 */
public class SignUpActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, AlertDialogUtils.OnButtonClickListener {

    String TAG = SignUpActivity.class.getSimpleName();
    private EditText edtName;
    private EditText edtEmail;
    private TextView edtDob;
    private EditText edtPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        edtName = (EditText) findViewById(R.id.edtName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtDob = (TextView) findViewById(R.id.edtDob);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        edtDob.setOnClickListener(this);
        findViewById(R.id.txtLogin).setOnClickListener(this);
        findViewById(R.id.txtSignUp).setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtLogin:
                break;
            case R.id.txtSignUp:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_REGISTER, true);
                }
                break;
            case R.id.edtDob:
                openDatePickerDialog();
                break;
            default:
                super.onClick(view);

        }
    }

    private void openDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        MyDatePickerDialog myDatePickerDialog = new MyDatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        myDatePickerDialog.setPermanentTitle("Please Select your Date of Birth");
        myDatePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
        Calendar calDOB = Calendar.getInstance();
        calDOB.set(arg1, arg2, arg3);
        arg2 = arg2 + 1;
        String birthday = arg2 + "/" + (arg3) + "/" + arg1;
        validateUserSelectionAge(calDOB.getTime(), birthday);
    }

    private void validateUserSelectionAge(Date birthDate, String birthString) {
        int age = DateUtils.calculateAge(birthDate);
        if (age >= 18) {
            edtDob.setText(birthString);
        } else {
            AlertDialogUtils.showAlertDialog(this, "", "Oops! You are too young to use Oye Caption", this);
        }
    }

    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtName.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter your name");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtEmail.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter your email");
            return false;
        } else if (edtDob.getText().toString().trim().equals("Date of Birth")) {
            ToastUtils.showToast(this, "Please select your date of birth");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString().trim())) {
            ToastUtils.showToast(this, "Please enter password");
            return false;
        } else if (edtPassword.getText().toString().trim().length() < 6) {
            ToastUtils.showToast(this, "Please enter a minimum of 6 character password");
            return false;
        }
        return true;

    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getApplicationContext())) {
            ToastUtils.showToast(getApplicationContext(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getApplicationContext()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            String url;
            switch (reqType) {
                case ApiConstants.REQUEST_REGISTER:
                    url = ApiConstants.URL_REGISTER;
                    className = BalanceHistoryResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_REGISTER) {
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setEmail(edtEmail.getText().toString().trim());
            registerRequest.setDob(edtDob.getText().toString().trim());
            registerRequest.setFirst_name(edtName.getText().toString().trim());
            //   registerRequest.setLast_name(edtPassword.getText().toString().trim());
            registerRequest.setPassword(edtPassword.getText().toString().trim());

            return new Gson().toJson(registerRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_REGISTER:
                    RegisterResponse registerResponse = (RegisterResponse) responseObject;
                    OyeCapPrefernece.getInstance().setLoggedIn(true);
                    OyeCapPrefernece.getInstance().setUserId(registerResponse.getUser_id());
                    OyeCapPrefernece.getInstance().setToken(registerResponse.getToken());
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onButtonClick(int buttonId) {

    }
}
