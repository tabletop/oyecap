package com.oyecaptain.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ApplicationUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.FacebookLoginRequest;
import com.oyecaptain.model.request.GoogleLoginRequest;
import com.oyecaptain.model.response.LoginResponse;
import com.oyecaptain.utils.OyeCapPrefernece;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by daemonn on 20/05/16.
 */
public class SplashActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    String TAG = SplashActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    private static final int REQ_SIGN_IN_REQUIRED = 55664;

    public CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private String url;
    private String mFacebookToken;
    String scopes = "oauth2:profile email";
    String token = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ApplicationUtils.printHashKey(this);

        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();

        if (OyeCapPrefernece.getInstance().getLoggedIn()) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        //Default Login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing Google Api
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //.setScopes(gso.getScopeArray());


        findViewById(R.id.imgFbLogin).setOnClickListener(this);
        findViewById(R.id.imgGoogleLogin).setOnClickListener(this);
        findViewById(R.id.txtLogin).setOnClickListener(this);
        findViewById(R.id.txtSignUp).setOnClickListener(this);
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN_FACEBOOK:
                    url = ApiConstants.URL_LOGIN;
                    className = LoginResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_LOGIN_GOOGLE:
                    url = ApiConstants.URL_LOGIN;
                    className = LoginResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_LOGIN_FACEBOOK) {
            FacebookLoginRequest facebookLoginRequest = new FacebookLoginRequest();
            facebookLoginRequest.setFacebook_access_token(mFacebookToken);
            return new Gson().toJson(facebookLoginRequest);
        } else if (reqType == ApiConstants.REQUEST_LOGIN_GOOGLE) {
            GoogleLoginRequest googleLoginRequest = new GoogleLoginRequest();
            googleLoginRequest.setGoogle_access_token(token);
            return new Gson().toJson(googleLoginRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            Intent intent;
            LoginResponse loginResponse;
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN_FACEBOOK:
                    loginResponse = (LoginResponse) responseObject;
                    OyeCapPrefernece.getInstance().setLoggedIn(true);
                    OyeCapPrefernece.getInstance().setUserId(loginResponse.getUser_id());
                    OyeCapPrefernece.getInstance().setToken(loginResponse.getToken());
                    intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case ApiConstants.REQUEST_LOGIN_GOOGLE:
                    loginResponse = (LoginResponse) responseObject;
                    OyeCapPrefernece.getInstance().setLoggedIn(true);
                    OyeCapPrefernece.getInstance().setUserId(loginResponse.getUser_id());
                    OyeCapPrefernece.getInstance().setToken(loginResponse.getToken());
                    intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.imgFbLogin:
                loginViaFb();
                break;
            case R.id.imgGoogleLogin:
                signIn();
                break;
            case R.id.txtLogin:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.txtSignUp:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                break;
            default:
                super.onClick(view);

        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private class RetrieveTokenTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String accountName = params[0];
            String scopes = "oauth2:profile email";
            try {
                token = GoogleAuthUtil.getToken(getApplicationContext(), accountName, scopes);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), REQ_SIGN_IN_REQUIRED);
            } catch (GoogleAuthException e) {
                Log.e(TAG, e.getMessage());
            }

            if (!StringUtils.isNullOrEmpty(token)) {
                hitApiRequest(ApiConstants.REQUEST_LOGIN_GOOGLE, true);
            }

            return token;
        }
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String token = null;
            Log.e(TAG, "display name: " + acct.getDisplayName());
            String personName = acct.getDisplayName();
            String email = acct.getEmail();
            if (email != null) {
                RetrieveTokenTask retrieveTokenTask = new RetrieveTokenTask();
                retrieveTokenTask.execute(email);
            }
            Log.e(TAG, "Name: " + personName + ", email: " + email + " " + token);


        }
    }

    private void loginViaFb() {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Log.d(TAG, accessToken.getToken());
                mFacebookToken = accessToken.getToken();
                hitApiRequest(ApiConstants.REQUEST_LOGIN_FACEBOOK, true);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "FB Login Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "FB Login Error:" + error);
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(this, getPermissions());
    }

    private ArrayList<String> getPermissions() {
        ArrayList<String> permission = new ArrayList<>();
        permission.add("email");
        permission.add("user_birthday");
        return permission;
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

}
