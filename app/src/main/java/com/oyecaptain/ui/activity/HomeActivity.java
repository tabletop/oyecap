package com.oyecaptain.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.TableLayout;

import com.krapps.ui.BaseActivity;
import com.oyecaptain.R;
import com.oyecaptain.ui.adapter.HomePagerAdapter;
import com.oyecaptain.ui.fragment.MyContestFragment;
import com.oyecaptain.utils.CustomViewPager;

/**
 * Created by daemonn on 20/05/16.
 */
public class HomeActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    String TAG = HomeActivity.class.getSimpleName();

    private TabLayout slidingTabLayout;
    private CustomViewPager viewPager;
    private HomePagerAdapter homePagerAdapter;
    private int unSelectedTabPosition;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setUpSlidingTabLayout();

    }

    private void setUpSlidingTabLayout() {
        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(5);
        homePagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(homePagerAdapter);


        slidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setOnTabSelectedListener(this);
        // Iterate over all tabs and set the custom view
        for (int i = 0; i < slidingTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = slidingTabLayout.getTabAt(i);
            tab.setCustomView(homePagerAdapter.getTabView(i));
        }
        //viewPager.setCurrentItem(0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                super.onClick(view);

        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void setCurrent(int value) {
        viewPager.setCurrentItem(value);
        TabLayout.Tab tab = slidingTabLayout.getTabAt(value);
        tab.select();
    }

    public void openMyGamesUpcoming() {
        setCurrent(2);
        ((MyContestFragment) homePagerAdapter.getCurrentFragment(2)).setCurrent(1);
    }
}
