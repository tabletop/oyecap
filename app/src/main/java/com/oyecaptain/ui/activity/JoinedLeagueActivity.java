package com.oyecaptain.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.ui.adapter.JoinedLeaguePagerAdapter;
import com.oyecaptain.ui.adapter.LeagueSelectionPagerAdapter;

/**
 * Created by daemonn on 20/05/16.
 */
public class JoinedLeagueActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    String TAG = JoinedLeagueActivity.class.getSimpleName();

    private TabLayout slidingTabLayout;
    private ViewPager viewPager;
    private String url;
    private JoinedLeaguePagerAdapter joinedLeaguePagerAdapter;
    private String matchname;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joined_league);

        Intent intent = getIntent();
        if (intent != null) {
            matchname = intent.getStringExtra(AppConstants.EXTRA_MATCH_NAME);
        }

        setupBackToolBar(matchname);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);
        joinedLeaguePagerAdapter = new JoinedLeaguePagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(joinedLeaguePagerAdapter);

        setUpSlidingTabLayout();

    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_JOINED_LEAGUES) {
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                super.onClick(view);

        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setUpSlidingTabLayout() {
        slidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        // slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setBackgroundResource(R.color.colorPrimary);
    }
}
