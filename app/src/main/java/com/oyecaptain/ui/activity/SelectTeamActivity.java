package com.oyecaptain.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.krapps.ui.BaseActivity;
import com.oyecaptain.R;
import com.oyecaptain.ui.adapter.TeamPagerAdapter;
import com.oyecaptain.ui.adapter.WalletPagerAdapter;

/**
 * Created by daemonn on 20/05/16.
 */
public class SelectTeamActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    String TAG = SelectTeamActivity.class.getSimpleName();

    private TabLayout slidingTabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private String url;
    private TeamPagerAdapter teamPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_select);

        setupBackToolBar("SELECT TEAM");

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);
        teamPagerAdapter = new TeamPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(teamPagerAdapter);

        setUpSlidingTabLayout();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                super.onClick(view);

        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setUpSlidingTabLayout() {
        slidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        // slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setBackgroundResource(R.color.colorPrimary);
    }
}
