package com.oyecaptain.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.oyecaptain.R;

@SuppressLint("ValidFragment")
public class OtpDialog extends DialogFragment implements OnClickListener {

    private String mObjectId;
    private OnClickListener mClickListener;
    private String message;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        return super.onCreateDialog(savedInstanceState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title and frame from dialog-fragment
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialogDark);
    }

    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogFadeAnimation;
        getDialog().setCanceledOnTouchOutside(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.otp_dialog, container, false);


        return view;
    }

    @Override
    public void onClick(View pClickSource) {
        switch (pClickSource.getId()) {

        }
    }

    public void setData(String objectId, OnClickListener onClickListener) {
        mClickListener = onClickListener;
        mObjectId = objectId;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}