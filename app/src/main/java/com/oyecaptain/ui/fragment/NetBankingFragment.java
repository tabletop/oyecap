package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.PayURequest;
import com.oyecaptain.model.response.HashResponse;
import com.oyecaptain.utils.OyeCapPrefernece;
import com.payUMoney.sdk.PayUmoneySdkInitilizer;
import com.payUMoney.sdk.SdkConstants;

import org.json.JSONException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by daemonn on 24/05/16.
 */
public class NetBankingFragment extends BaseFragment {


    private String TAG = NetBankingFragment.class.getSimpleName();
    private View mView;
    private EditText edtAmount;
    private PayURequest payURequest;
    private String salt = "yuCGD0eCKK";
    private String key = "sLiSS1qy";
    private String merchantId = "5426523";
    private String productIdfo = "recharge";
    private PayUmoneySdkInitilizer.PaymentParam mPaymentParam;
    private String txnId = "6OzfouddRN";
    private PayUmoneySdkInitilizer.PaymentParam paymentParam;
    private float amount = 1.0f;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_net_banking, null);
        txnId = txnId + System.currentTimeMillis();

        edtAmount = (EditText) mView.findViewById(R.id.edtAmount);

        mView.findViewById(R.id.addFunds).setOnClickListener(this);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            String url;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GENRETE_HASH:
                    url = ApiConstants.URL_GENERATE_HASH;
                    className = HashResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    }, getJsonString(reqType));
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GENRETE_HASH) {
            payURequest = new PayURequest();
            payURequest.setEmail(OyeCapPrefernece.getInstance().getEmail());
            payURequest.setAmount(amount + "");
            payURequest.setFirstname(OyeCapPrefernece.getInstance().getName());
            payURequest.setProductinfo(productIdfo);
            payURequest.setStatus("");
            payURequest.setTxnid(txnId);
            payURequest.setUdf1("");
            payURequest.setUdf2("");
            payURequest.setUdf3("");
            payURequest.setUdf4("");
            payURequest.setUdf5("");
            return new Gson().toJson(payURequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GENRETE_HASH:
                    HashResponse hashResponse = (HashResponse) responseObject;
                    if (hashResponse.getHash() != null) {
                        String hashSequence = key + "|" + txnId + "|" + amount + "|" + productIdfo + "|" + OyeCapPrefernece.getInstance().getName() + "|"
                                + OyeCapPrefernece.getInstance().getEmail() + "||||||" + salt;
                        paymentParam.setMerchantHash(hashCal(hashSequence));
                        PayUmoneySdkInitilizer.startPaymentActivityForResult(getActivity(), paymentParam);

                        ToastUtils.showToast(getActivity(), hashResponse.getString());
                    } else {
                        ToastUtils.showToast(getActivity(), "Value is not coming");
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void makePayment() {

        PayUmoneySdkInitilizer.PaymentParam.Builder builder = new PayUmoneySdkInitilizer.PaymentParam.Builder();
        builder.setAmount(amount)
                .setTnxId(txnId)
                .setPhone("9555389674")
                .setProductName(productIdfo)
                .setFirstName(OyeCapPrefernece.getInstance().getName())
                .setEmail(OyeCapPrefernece.getInstance().getEmail())
                .setsUrl("https://test.oyecaptain.com/balance/payu_success")
                .setfUrl("https://test.oyecaptain.com/balance/payu_failure")
                .setUdf1("")
                .setUdf2("")
                .setUdf3("")
                .setUdf4("")
                .setUdf5("")
                .setIsDebug(false)
                .setKey(key)
                .setMerchantId(merchantId);// Debug Merchant ID

        paymentParam = builder.build();

        hitApiRequest(ApiConstants.REQUEST_GENRETE_HASH, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PayUmoneySdkInitilizer.PAYU_SDK_PAYMENT_REQUEST_CODE) {


            if (resultCode == getActivity().RESULT_OK) {
                Log.i(TAG, "Success - Payment ID : " + data.getStringExtra(SdkConstants.PAYMENT_ID));
                String paymentId = data.getStringExtra(SdkConstants.PAYMENT_ID);
                ToastUtils.showToast(getActivity(), "Payment Success Id : " + paymentId);
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                Log.i(TAG, "failure");
                ToastUtils.showToast(getActivity(), "cancelled");
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_FAILED) {
                Log.i("app_activity", "failure");

                if (data != null) {
                    if (data.getStringExtra(SdkConstants.RESULT).equals("cancel")) {

                    } else {
                        ToastUtils.showToast(getActivity(), "failure");
                    }
                }
                //Write your code if there's no result
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_BACK) {
                Log.i(TAG, "User returned without login");
                ToastUtils.showToast(getActivity(), "User returned without login");
            }
        }
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addFunds:
                if (validate()) {
                    makePayment();
                }
                break;
            default:
                super.onClick(view);
        }
    }

    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtAmount.getText().toString().trim())) {
            ToastUtils.showToast(getActivity(), "Please enter amount");
            return false;
        }
        return true;
    }

}
