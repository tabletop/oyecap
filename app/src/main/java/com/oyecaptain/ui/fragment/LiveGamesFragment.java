package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.MegaLeagues;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.ui.activity.JoinedLeagueActivity;
import com.oyecaptain.ui.activity.LeagueSelectionActivity;
import com.oyecaptain.ui.adapter.LiveGamesRecyclerAdapter;
import com.oyecaptain.utils.OyeCapPrefernece;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class LiveGamesFragment extends BaseFragment {

    private String TAG = LiveGamesFragment.class.getSimpleName();
    private View mView;
    private Handler timerHandler = new Handler();
    private String url;
    private String mTournamentId;
    private RecyclerView listview;
    private LiveGamesRecyclerAdapter liveGamesRecyclerAdapter;

    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            liveGamesRecyclerAdapter.notifyDataSetChanged();
            timerHandler.postDelayed(this, 1000);
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_live_games, null);


        listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);
        liveGamesRecyclerAdapter = new LiveGamesRecyclerAdapter(getActivity(), this);
        listview.setAdapter(liveGamesRecyclerAdapter);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_TOURNAMENTS:
                    url = ApiConstants.URL_TOURNAMENTS;
                    className = TournamentResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_GET_MATCHES:
                    url = String.format(ApiConstants.URL_MATCHES, mTournamentId);
                    className = MatchesResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_TOURNAMENTS) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_TOURNAMENTS:
                    TournamentResponse tournamentResponse = (TournamentResponse) responseObject;
                    if (tournamentResponse != null && tournamentResponse.getTournaments() != null) {
                        setTournamentAdapter(tournamentResponse.getTournaments());
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setTournamentAdapter(List<Tournaments> tournaments) {
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtPointCard:
                Matches matches = (Matches) view.getTag();
                String url = ApiConstants.URL_WEB + "/mycontest/scorecard1/" + matches.getTournament() + "/" + matches.getId() + "/" + OyeCapPrefernece.getInstance().getUserId();
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break;
            case R.id.txtScoreCard:
                Matches matches1 = (Matches) view.getTag();
                String url1 = ApiConstants.URL_WEB + "/mycontest/livescorecard1/" + matches1.getId() + "/" + OyeCapPrefernece.getInstance().getUserId();
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url1));
                startActivity(intent);
                break;
            case R.id.lnrTeams:
                Matches matches2 = (Matches) view.getTag();
                intent = new Intent(getActivity(), JoinedLeagueActivity.class);
                intent.putExtra(AppConstants.EXTRA_MATCH_ID, matches2.getId() + "");
                intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matches2.getName());
                intent.putExtra(AppConstants.EXTRA_SHOW_JOIN_MORE_LEAGUE, false);
                intent.putExtra(AppConstants.EXTRA_MATCH, matches2);
                startActivity(intent);
                break;
            default:
                super.onClick(view);
        }

    }


    public void setMatchesAdapter(List<Matches> liveMatches) {
        liveGamesRecyclerAdapter.setListData(liveMatches);
        liveGamesRecyclerAdapter.notifyDataSetChanged();
        timerHandler.postDelayed(timerRunnable, 1000);
    }
}
