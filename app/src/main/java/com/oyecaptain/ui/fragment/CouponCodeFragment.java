package com.oyecaptain.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.CouponRequest;
import com.oyecaptain.model.response.AddMoneyResponse;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;

import org.json.JSONException;

/**
 * Created by daemonn on 24/05/16.
 */
public class CouponCodeFragment extends BaseFragment {


    private String TAG = CouponCodeFragment.class.getSimpleName();
    private View mView;
    private EditText edtCouponCode;
    private String url;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_coupon_code, null);

        edtCouponCode = (EditText) mView.findViewById(R.id.edtCouponCode);


        mView.findViewById(R.id.addFunds).setOnClickListener(this);
        return mView;
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_ADD_MONEY:
                    url = ApiConstants.URL_ADD_MONEY;
                    className = AddMoneyResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    }, getJsonString(ApiConstants.REQUEST_ADD_MONEY));
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_ADD_MONEY) {
            CouponRequest couponRequest = new CouponRequest();
            couponRequest.setMedium("coupon");
            couponRequest.setCoupon(edtCouponCode.getText().toString().trim());
            return new Gson().toJson(couponRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_ADD_MONEY:
                    AddMoneyResponse addMoneyResponse = (AddMoneyResponse) responseObject;
                    if (addMoneyResponse.isSuccess()) {
                        ToastUtils.showToast(getActivity(), "Coupon successfully applied!");
                    } else {
                        ToastUtils.showToast(getActivity(), addMoneyResponse.getMessage());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addFunds:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_ADD_MONEY, true);
                }
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtCouponCode.getText().toString().trim())) {
            ToastUtils.showToast(getActivity(), "Please enter coupon code");
            return false;
        }
        return true;
    }
}
