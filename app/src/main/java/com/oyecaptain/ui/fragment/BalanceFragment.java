package com.oyecaptain.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.response.Balance;
import com.oyecaptain.model.response.BalanceResponse;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.ui.adapter.LiveGamesRecyclerAdapter;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class BalanceFragment extends BaseFragment {

    private String TAG = BalanceFragment.class.getSimpleName();
    private View mView;
    private String url;
    private TextView txtUnutillized, txtWinning, txtBonus, txtTotal;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_balance, null);

        txtUnutillized = (TextView) mView.findViewById(R.id.txtUnutillized);
        txtWinning = (TextView) mView.findViewById(R.id.txtWinning);
        txtBonus = (TextView) mView.findViewById(R.id.txtBonus);
        txtTotal = (TextView) mView.findViewById(R.id.txtTotal);
        hitApiRequest(ApiConstants.REQUEST_GET_BALANCE, true);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_BALANCE:
                    url = ApiConstants.URL_BALANCE;
                    className = BalanceResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_BALANCE) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_BALANCE:
                    BalanceResponse balanceResponse = (BalanceResponse) responseObject;
                    if (balanceResponse != null) {
                        updateDataToUi(balanceResponse.getBalance());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void updateDataToUi(Balance balance) {
        txtTotal.setText("\u20B9 " + (int) balance.getBalance());
        txtBonus.setText("\u20B9 " + (int) balance.getBonus());
        txtUnutillized.setText("\u20B9 " + (int) balance.getUnutilized());
        txtWinning.setText("\u20B9 " + (int) balance.getWinning());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                super.onClick(view);
        }

    }


}
