package com.oyecaptain.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.response.JoinedMatchesResponse;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.ui.adapter.MyGamesAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class MyContestFragment extends BaseFragment {

    private String TAG = MyContestFragment.class.getSimpleName();
    private View mView;
    private TabLayout slidingTabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private String url;
    private MyGamesAdapter myGamesAdapter;
    private boolean mIsVisibleToUser;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_my_contest, null);

        Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity) getActivity()).getSupportActionBar().setTitle("MY GAMES");
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).setCurrent(1);
            }
        });*/

        viewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        myGamesAdapter = new MyGamesAdapter(getChildFragmentManager(), getActivity());
        viewPager.setAdapter(myGamesAdapter);

        setUpSlidingTabLayout();


        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (mIsVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hitApiRequest(ApiConstants.REQUEST_GET_JOINED_MATCHES, false);
                }
            }, 500);
        }

    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_JOINED_MATCHES:
                    url = ApiConstants.URL_JOINED_MATCHES;
                    className = JoinedMatchesResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:

                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_JOINED_MATCHES) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_JOINED_MATCHES:
                    JoinedMatchesResponse joinedMatchesResponse = (JoinedMatchesResponse) responseObject;
                    if (joinedMatchesResponse != null && joinedMatchesResponse.getMatches() != null) {
                        getGames(joinedMatchesResponse.getMatches());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void getGames(List<Matches> matches) {
        List<Matches> liveMatches = new ArrayList<>();
        List<Matches> pastMatches = new ArrayList<>();
        List<Matches> upcomningMatches = new ArrayList<>();
        for (Matches temp : matches) {
            if (temp.getStatus() == 1) {
                liveMatches.add(temp);
            } else if (temp.getStatus() == 2) {
                upcomningMatches.add(temp);
            } else {
                pastMatches.add(temp);
            }
        }

        ((LiveGamesFragment) myGamesAdapter.getCurrentFragment(0)).setMatchesAdapter(liveMatches);
        ((UpcomingGamesFragment) myGamesAdapter.getCurrentFragment(1)).setMatchesAdapter(upcomningMatches);
        ((PastGamesFragment) myGamesAdapter.getCurrentFragment(2)).setMatchesAdapter(pastMatches);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtWallet:
                break;
            default:
                super.onClick(view);
        }

    }

    private void setUpSlidingTabLayout() {
        slidingTabLayout = (TabLayout) mView.findViewById(R.id.sliding_tabs);
        // slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setupWithViewPager(viewPager);
        slidingTabLayout.setBackgroundResource(R.color.colorPrimary);
    }

    public void setCurrent(int pos) {
        viewPager.setCurrentItem(pos);
    }


}
