package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.LogoutRequest;
import com.oyecaptain.model.response.LogoutResponse;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.User;
import com.oyecaptain.model.response.UserDetailResponse;
import com.oyecaptain.ui.activity.SplashActivity;
import com.oyecaptain.ui.activity.WalletActivity;
import com.oyecaptain.utils.OyeCapPrefernece;

import org.json.JSONException;


/**
 * Created by daemonn on 16/04/16.
 */
public class SettingFragment extends BaseFragment {

    private String TAG = SettingFragment.class.getSimpleName();
    private View mView;
    private String url;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_settings, null);

        Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        toolbar.setTitle("SETTINGS");
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mView.findViewById(R.id.txtWallet).setOnClickListener(this);

        mView.findViewById(R.id.txtWallet).setOnClickListener(this);
        mView.findViewById(R.id.txtHowToPlay).setOnClickListener(this);
        mView.findViewById(R.id.txtPointSystem).setOnClickListener(this);
        mView.findViewById(R.id.txtShare).setOnClickListener(this);
        mView.findViewById(R.id.txtLogout).setOnClickListener(this);

        hitApiRequest(ApiConstants.REQUEST_GET_USER_DETAILS, false);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_LOGOUT:
                    url = ApiConstants.URL_LOGOUT;
                    className = LogoutResponse.class;
                    request = VolleyJsonRequest.dodelete(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    }, getJsonString(ApiConstants.REQUEST_LOGOUT));
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_GET_USER_DETAILS:
                    url = ApiConstants.URL_USER_DETAILS;
                    className = UserDetailResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_LOGOUT) {
            new Gson().toJson(new LogoutRequest());
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_LOGOUT:
                    LogoutResponse logoutResponse = (LogoutResponse) responseObject;
                    OyeCapPrefernece.getInstance().clear();
                    if (logoutResponse.isSuccess() == true) {
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    break;
                case ApiConstants.REQUEST_GET_USER_DETAILS:
                    UserDetailResponse userDetailResponse = (UserDetailResponse) responseObject;
                    if (userDetailResponse != null && userDetailResponse.getUser() != null) {
                        updateDataToUi(userDetailResponse.getUser());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void updateDataToUi(User user) {
        ((TextView) mView.findViewById(R.id.txtName)).setText(user.getScreen_name());
        ((TextView) mView.findViewById(R.id.txtEmail)).setText(user.getUser_email());

        OyeCapPrefernece.getInstance().setEmail(user.getUser_email());
        OyeCapPrefernece.getInstance().setName(user.getUser_name());

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtWallet:
                intent = new Intent(getActivity(), WalletActivity.class);
                startActivity(intent);
                break;
            case R.id.txtHowToPlay:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(intent);
                break;
            case R.id.txtPointSystem:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(intent);
                break;
            case R.id.txtShare:
                break;
            case R.id.txtLogout:
                hitApiRequest(ApiConstants.REQUEST_LOGOUT, true);
                break;
            default:
                super.onClick(view);
        }

    }


}
