package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.ui.activity.JoinedLeagueActivity;
import com.oyecaptain.ui.activity.LeagueSelectionActivity;
import com.oyecaptain.ui.adapter.LiveGamesRecyclerAdapter;
import com.oyecaptain.utils.OyeCapPrefernece;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class PastGamesFragment extends BaseFragment {

    private String TAG = PastGamesFragment.class.getSimpleName();
    private View mView;
    private String url;
    private String mTournamentId;
    private RecyclerView listview;
    private LiveGamesRecyclerAdapter liveGamesRecyclerAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_past_games, null);


        listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);
        liveGamesRecyclerAdapter = new LiveGamesRecyclerAdapter(getActivity(), this);
        listview.setAdapter(liveGamesRecyclerAdapter);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {

    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_TOURNAMENTS) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            switch (reqType) {
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtPointCard:
                Matches matches = (Matches) view.getTag();
                String url = ApiConstants.URL_WEB + "/mycontest/scorecard1/" + matches.getTournament() + "/" + matches.getId() + "/" + OyeCapPrefernece.getInstance().getUserId();
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break;
            case R.id.txtScoreCard:
                Matches matches1 = (Matches) view.getTag();
                String url1 = ApiConstants.URL_WEB + "/mycontest/livescorecard1/" + matches1.getId() + "/" + OyeCapPrefernece.getInstance().getUserId();
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url1));
                startActivity(intent);
                break;
            case R.id.lnrTeams:
                Matches matches2 = (Matches) view.getTag();
                intent = new Intent(getActivity(), JoinedLeagueActivity.class);
                intent.putExtra(AppConstants.EXTRA_MATCH_ID, matches2.getId() + "");
                intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matches2.getName());
                intent.putExtra(AppConstants.EXTRA_MATCH, matches2);
                startActivity(intent);
                break;
            default:
                super.onClick(view);
        }

    }


    public void setMatchesAdapter(List<Matches> pastMatches) {
        liveGamesRecyclerAdapter.setListData(pastMatches);
        liveGamesRecyclerAdapter.notifyDataSetChanged();

    }
}
