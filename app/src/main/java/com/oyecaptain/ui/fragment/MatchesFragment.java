package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.ui.activity.HomeActivity;
import com.oyecaptain.ui.activity.SelectTeamActivity;
import com.oyecaptain.ui.adapter.MatchesRecyclerAdapter;
import com.oyecaptain.ui.adapter.TournamentAdapter;
import com.oyecaptain.utils.OyeCapPrefernece;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class MatchesFragment extends BaseFragment {

    private String TAG = MatchesFragment.class.getSimpleName();
    private View mView;
    private String url;
    private Handler timerHandler = new Handler();
    private String mTournamentId;
    private GridView gridview;
    private RecyclerView listview;
    private TournamentAdapter tournamentAdapter;
    private MatchesRecyclerAdapter matchesRecyclerAdapter;


    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            matchesRecyclerAdapter.notifyDataSetChanged();
            timerHandler.postDelayed(this, 1000);
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_matches, null);

        Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        toolbar.setTitle("Matches");
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gridview = (GridView) mView.findViewById(R.id.gridview);
        tournamentAdapter = new TournamentAdapter(getActivity(), this);
        gridview.setAdapter(tournamentAdapter);


        listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);
        matchesRecyclerAdapter = new MatchesRecyclerAdapter(getActivity(), this);
        listview.setAdapter(matchesRecyclerAdapter);


        Log.d("token", OyeCapPrefernece.getInstance().getToken());
        hitApiRequest(ApiConstants.REQUEST_GET_TOURNAMENTS, true);
        hitApiRequest(ApiConstants.REQUEST_GET_MATCHES, false);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_TOURNAMENTS:
                    url = ApiConstants.URL_TOURNAMENTS;
                    className = TournamentResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_GET_MATCHES:
                    url = String.format(ApiConstants.URL_MATCHES, mTournamentId);
                    className = MatchesResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_TOURNAMENTS) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_TOURNAMENTS:
                    TournamentResponse tournamentResponse = (TournamentResponse) responseObject;
                    if (tournamentResponse != null && tournamentResponse.getTournaments() != null) {
                        setTournamentAdapter(tournamentResponse.getTournaments());
                    }

                    break;
                case ApiConstants.REQUEST_GET_MATCHES:
                    MatchesResponse matchesResponse = (MatchesResponse) responseObject;
                    if (matchesResponse != null && matchesResponse.getMatches() != null) {
                        setMatchesAdapter(matchesResponse.getMatches());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setMatchesAdapter(List<Matches> matches) {
        timerHandler.postDelayed(timerRunnable, 1000);
        matchesRecyclerAdapter.setListData(matches);
        matchesRecyclerAdapter.notifyDataSetChanged();
    }

    private void setTournamentAdapter(List<Tournaments> tournaments) {
        if (tournaments.size() > 0) {
            mTournamentId = tournaments.get(0).getId() + "";
            hitApiRequest(ApiConstants.REQUEST_GET_MATCHES, false);
            tournamentAdapter.setListData(tournaments);
            tournamentAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtName:
                Tournaments tournaments = (Tournaments) view.getTag();
                mTournamentId = tournaments.getId() + "";
                hitApiRequest(ApiConstants.REQUEST_GET_MATCHES, true);
                break;
            case R.id.txtWin:
                Matches matches = (Matches) view.getTag();
                intent = new Intent(getActivity(), SelectTeamActivity.class);
                intent.putExtra(AppConstants.EXTRA_TOURNAMENT_ID, mTournamentId);
                intent.putExtra(AppConstants.EXTRA_MATCH_ID, matches.getId() + "");
                intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matches.getName() + "");
                intent.putExtra(AppConstants.EXTRA_MATCH, matches);
                startActivity(intent);

                /*if (matches.isJoined() == true) {
                    ((HomeActivity) getActivity()).openMyGamesUpcoming();
                } else {
                    intent = new Intent(getActivity(), SelectTeamActivity.class);
                    intent.putExtra(AppConstants.EXTRA_TOURNAMENT_ID, mTournamentId);
                    intent.putExtra(AppConstants.EXTRA_MATCH_ID, matches.getId() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH, matches);
                    startActivity(intent);

                }*/
            default:
                super.onClick(view);
        }

    }


}
