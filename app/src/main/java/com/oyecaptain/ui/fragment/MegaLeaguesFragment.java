package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.model.CommonJsonResponse;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.request.JoinLeagueRequest;
import com.oyecaptain.model.response.CreateLeagueResponse;
import com.oyecaptain.model.response.InviteCodeResponse;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MegaLeagueResponse;
import com.oyecaptain.model.response.MegaLeagues;
import com.oyecaptain.ui.activity.JoinedLeagueActivity;
import com.oyecaptain.ui.activity.LeagueSelectionActivity;
import com.oyecaptain.ui.activity.SelectTeamActivity;
import com.oyecaptain.ui.adapter.MegaLeagueRecyclerAdapter;
import com.oyecaptain.utils.ObjectDto;
import com.oyecaptain.utils.ShareIntentUtil;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;

import java.util.List;


/**
 * Created by daemonn on 16/04/16.
 */
public class MegaLeaguesFragment extends BaseFragment {

    private String TAG = MegaLeaguesFragment.class.getSimpleName();
    private View mView;
    private String url;
    private Handler timerHandler = new Handler();
    private RecyclerView listview;
    private MegaLeagueRecyclerAdapter megaLeagueRecyclerAdapter;
    private int mLeagueId;
    private int matchId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_mega_leagues, null);

        Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        toolbar.setTitle("MEGA LEAGUES");
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);
        megaLeagueRecyclerAdapter = new MegaLeagueRecyclerAdapter(getActivity(), this);
        listview.setAdapter(megaLeagueRecyclerAdapter);

        hitApiRequest(ApiConstants.REQUEST_GET_MEGA_LEAGUES, false);

        return mView;
    }

    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            megaLeagueRecyclerAdapter.notifyDataSetChanged();
            timerHandler.postDelayed(this, 1000);
        }
    };


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_MEGA_LEAGUES:
                    url = ApiConstants.URL_MEGA_LEAGUES;
                    className = MegaLeagueResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_INVITE_CODE:
                    ((BaseActivity) getActivity()).showProgressDialog();
                    url = String.format(ApiConstants.URL_INVITE_CODE, mLeagueId);
                    className = InviteCodeResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_JOIN_LEAGUE:
                    ((BaseActivity) getActivity()).showProgressDialog();
                    url = String.format(ApiConstants.URL_JOIN_LEAGUE, mLeagueId, matchId);
                    className = CommonJsonResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    }, getJsonString(ApiConstants.REQUEST_JOIN_LEAGUE));
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_JOIN_LEAGUE) {
            JoinLeagueRequest joinLeagueRequest = new JoinLeagueRequest();
            return new Gson().toJson(joinLeagueRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_MEGA_LEAGUES:
                    MegaLeagueResponse megaLeagueResponse = (MegaLeagueResponse) responseObject;
                    if (megaLeagueResponse != null && megaLeagueResponse.getMegaLeagues() != null) {
                        setMegaLeagueAdapter(megaLeagueResponse.getMegaLeagues());
                    }
                    break;
                case ApiConstants.REQUEST_INVITE_CODE:
                    InviteCodeResponse inviteCodeResponse = (InviteCodeResponse) responseObject;
                    if (inviteCodeResponse != null) {
                        ShareIntentUtil.shareEvent(getActivity(), ApiConstants.URL_WEB + inviteCodeResponse.getUrl());
                    }
                    break;
                case ApiConstants.REQUEST_JOIN_LEAGUE:
                    CommonJsonResponse commonJsonResponse = (CommonJsonResponse) responseObject;
                    if (commonJsonResponse.isSuccess()) {
                        List<MegaLeagues> megaLeagues = megaLeagueRecyclerAdapter.getListdata();
                        MegaLeagues temp = new MegaLeagues();
                        temp.setId(mLeagueId);
                        if (megaLeagues.contains(temp)) {
                            int pos = megaLeagues.indexOf(temp);
                            megaLeagues.get(pos).setJoined(true);
                            megaLeagueRecyclerAdapter.notifyDataSetChanged();
                        }
                    }
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setMegaLeagueAdapter(List<MegaLeagues> megaLeagues) {
        timerHandler.postDelayed(timerRunnable, 1000);
        megaLeagueRecyclerAdapter.setListData(megaLeagues);
        megaLeagueRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.rtlparent:
                MegaLeagues megaLeagues = (MegaLeagues) view.getTag();
                if (megaLeagues.isTeam_sel() == true) {
                    intent = new Intent(getActivity(), JoinedLeagueActivity.class);
                    intent.putExtra(AppConstants.EXTRA_MATCH_ID, megaLeagues.getMatch_id() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH_NAME, megaLeagues.getMatch_name());
                    startActivity(intent);
                } else {
                    intent = new Intent(getActivity(), SelectTeamActivity.class);
                    intent.putExtra(AppConstants.EXTRA_TOURNAMENT_ID, megaLeagues.getTournament_id() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH_ID, megaLeagues.getMatch_id() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH_NAME, megaLeagues.getMatch_name() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH, ObjectDto.getMatchFromMegaLeague(megaLeagues));
                    startActivity(intent);
                }
                break;
            case R.id.txtInvite:
                MegaLeagues megaLeagues1 = (MegaLeagues) view.getTag();
                mLeagueId = megaLeagues1.getId();
                matchId = megaLeagues1.getMatch_id();
                if (megaLeagues1.isTeam_sel() == true) {
                    if (megaLeagues1.isJoined() == true) {
                        hitApiRequest(ApiConstants.REQUEST_INVITE_CODE, true);
                    } else {
                        hitApiRequest(ApiConstants.REQUEST_JOIN_LEAGUE, true);
                    }
                } else {
                    intent = new Intent(getActivity(), SelectTeamActivity.class);
                    intent.putExtra(AppConstants.EXTRA_TOURNAMENT_ID, megaLeagues1.getTournament_id() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH_ID, megaLeagues1.getMatch_id() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH_NAME, megaLeagues1.getMatch_name() + "");
                    intent.putExtra(AppConstants.EXTRA_MATCH, ObjectDto.getMatchFromMegaLeague(megaLeagues1));
                    startActivity(intent);
                }

                break;
            default:
                super.onClick(view);
        }

    }


}
