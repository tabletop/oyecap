package com.oyecaptain.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.request.CreatePrivateLeagueRequest;
import com.oyecaptain.model.response.CreateLeagueResponse;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.ui.activity.LeagueSelectionActivity;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class PrivateLeagueFragment extends BaseFragment {

    private String TAG = PrivateLeagueFragment.class.getSimpleName();
    private View mView;
    private String url;
    private EditText edtPrize;
    private EditText edtLeagueSize;
    private EditText edtEntryFee;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_private_league, null);

        edtPrize = (EditText) mView.findViewById(R.id.edtPrize);
        edtLeagueSize = (EditText) mView.findViewById(R.id.edtLeagueSize);
        edtEntryFee = (EditText) mView.findViewById(R.id.edtEntryFee);


        mView.findViewById(R.id.txtCreate).setOnClickListener(this);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_CREATE_PRIVATE_LEAGUE:
                    url = ApiConstants.URL_CREATE_PRIVATE_LEAGUE;
                    className = CreateLeagueResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    }, getJsonString(ApiConstants.REQUEST_CREATE_PRIVATE_LEAGUE));
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_CREATE_PRIVATE_LEAGUE) {
            CreatePrivateLeagueRequest createPrivateLeagueRequest = new CreatePrivateLeagueRequest();
            createPrivateLeagueRequest.setFee(edtEntryFee.getText().toString().trim());
            createPrivateLeagueRequest.setLeague_size(Integer.valueOf(edtLeagueSize.getText().toString().trim()));
            createPrivateLeagueRequest.setLive_in(System.currentTimeMillis());
            createPrivateLeagueRequest.setPrize(edtPrize.getText().toString());
            createPrivateLeagueRequest.setFixture_id(Integer.parseInt(((LeagueSelectionActivity) getActivity()).matchId));

            return new Gson().toJson(createPrivateLeagueRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_CREATE_PRIVATE_LEAGUE:
                    CreateLeagueResponse createLeagueResponse = (CreateLeagueResponse) responseObject;
                    if (createLeagueResponse != null && createLeagueResponse.isSuccess() == true) {

                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtCreate:
                if (validate()) {
                    hitApiRequest(ApiConstants.REQUEST_CREATE_PRIVATE_LEAGUE, true);
                }
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtPrize.getText().toString().trim())) {
            ToastUtils.showToast(getActivity(), "Please enter prize");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtEntryFee.getText().toString().trim())) {
            ToastUtils.showToast(getActivity(), "Please enter entry fee");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtLeagueSize.getText().toString().trim())) {
            ToastUtils.showToast(getActivity(), "Please enter league size");
            return false;
        }
        return true;
    }
}
