package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.model.CommonJsonResponse;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.request.JoinLeagueRequest;
import com.oyecaptain.model.response.InviteCodeResponse;
import com.oyecaptain.model.response.LeaguesCategories;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.PublicLeagueResponse;
import com.oyecaptain.model.response.PublicLeagues;
import com.oyecaptain.ui.activity.SelectTeamActivity;
import com.oyecaptain.ui.adapter.LiveGamesRecyclerAdapter;
import com.oyecaptain.utils.ShareIntentUtil;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class PublicLeagueFragment extends BaseFragment {

    private String TAG = PublicLeagueFragment.class.getSimpleName();
    private View mView;
    private String url;
    private String mTournamentId;
    private RecyclerView listview;
    private LiveGamesRecyclerAdapter liveGamesRecyclerAdapter;
    private LayoutInflater mInflater;
    private String matchId;
    private String matchname;
    private String mLeagueId;
    private Matches matches;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_public_league, null);
        mInflater = LayoutInflater.from(getActivity().getBaseContext());

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            matchId = intent.getStringExtra(AppConstants.EXTRA_MATCH_ID);
            matchname = intent.getStringExtra(AppConstants.EXTRA_MATCH_NAME);
        }

        hitApiRequest(ApiConstants.REQUEST_GET_LEAGUES, true);


        mView.findViewById(R.id.txtEditTeam).setOnClickListener(this);
        mView.findViewById(R.id.rtlEditTeam).setOnClickListener(this);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_LEAGUES:
                    url = String.format(ApiConstants.URL_LEAGUES, matchId);
                    className = PublicLeagueResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_INVITE_CODE:
                    ((BaseActivity) getActivity()).showProgressDialog();
                    url = String.format(ApiConstants.URL_INVITE_CODE, mLeagueId);
                    className = InviteCodeResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_JOIN_LEAGUE:
                    ((BaseActivity) getActivity()).showProgressDialog();
                    url = String.format(ApiConstants.URL_JOIN_LEAGUE, mLeagueId, matchId);
                    className = CommonJsonResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    }, getJsonString(ApiConstants.REQUEST_JOIN_LEAGUE));
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_JOIN_LEAGUE) {
            JoinLeagueRequest joinLeagueRequest = new JoinLeagueRequest();
            return new Gson().toJson(joinLeagueRequest);
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            VolleyJsonRequest request;
            Class className;

            switch (reqType) {
                case ApiConstants.REQUEST_GET_LEAGUES:
                    PublicLeagueResponse publicLeagueResponse = (PublicLeagueResponse) responseObject;
                    if (publicLeagueResponse != null && publicLeagueResponse.getMatch() != null && publicLeagueResponse.getMatch().getMegaLeagues() != null) {
                        addMegaLeagueList(publicLeagueResponse.getMatch());
                    }

                    if (publicLeagueResponse != null && publicLeagueResponse.getMatch() != null && publicLeagueResponse.getMatch().getLeaguesCategoriesList() != null) {
                        matches = publicLeagueResponse.getMatch();
                        addPublicLeagueList(publicLeagueResponse.getMatch());
                    }
                    break;
                case ApiConstants.REQUEST_INVITE_CODE:
                    ((BaseActivity) getActivity()).removeProgressDialog();
                    InviteCodeResponse inviteCodeResponse = (InviteCodeResponse) responseObject;
                    if (inviteCodeResponse != null) {
                        ShareIntentUtil.shareEvent(getActivity(), ApiConstants.URL_WEB + inviteCodeResponse.getUrl());
                    }
                    break;
                case ApiConstants.REQUEST_JOIN_LEAGUE:
                    ((BaseActivity) getActivity()).removeProgressDialog();
                    CommonJsonResponse commonJsonResponse = (CommonJsonResponse) responseObject;
                    if (commonJsonResponse.isSuccess()) {
                    }
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.rtlEditTeam:
            case R.id.txtEditTeam:
                intent = new Intent(getActivity(), SelectTeamActivity.class);
                intent.putExtra(AppConstants.EXTRA_TOURNAMENT_ID, mTournamentId + "");
                intent.putExtra(AppConstants.EXTRA_MATCH_ID, matchId + "");
                intent.putExtra(AppConstants.EXTRA_MATCH, matches);
                intent.putExtra(AppConstants.EXTRA_TEAM_EDITABLE, true);
                startActivity(intent);
                break;
            default:
                super.onClick(view);
        }

    }


    private void addMegaLeagueList(final Matches match) {
        List<PublicLeagues> publicLeagues = match.getMegaLeagues();
        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.lnrMegaLeague);
        for (int i = 0; i <= publicLeagues.size() - 1; i++) {
            if (publicLeagues.get(i).isMega() == true) {
                View rowView = mInflater.inflate(R.layout.list_item_league_selection, null);
                ((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.get(i).getWin() + "");
                ((TextView) rowView.findViewById(R.id.txtMembers)).setText(publicLeagues.get(i).getFilled_seats() + "/" + publicLeagues.get(i).getTotal_seats());
                ((TextView) rowView.findViewById(R.id.txtPaid)).setText(publicLeagues.get(i).getFee() + "");
                //((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.get(i).getWin() + "");

                if (publicLeagues.get(i).getTotal_seats() == publicLeagues.get(i).getFilled_seats()) {
                    ((TextView) rowView.findViewById(R.id.txtInvite)).setText("COMPLETE");
                } else {
                    if (publicLeagues.get(i).isJoined()) {
                        ((TextView) rowView.findViewById(R.id.txtInvite)).setText("INVITE");
                        rowView.findViewById(R.id.txtInvite).setTag(publicLeagues.get(i));
                        rowView.findViewById(R.id.txtInvite).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PublicLeagues temp = (PublicLeagues) v.getTag();
                                mLeagueId = temp.getId() + "";
                                matchId = temp.getMatch_id() + "";
                                hitApiRequest(ApiConstants.REQUEST_INVITE_CODE, true);
                            }
                        });
                    } else {
                        ((TextView) rowView.findViewById(R.id.txtInvite)).setText("JOIN");
                        rowView.findViewById(R.id.txtInvite).setTag(publicLeagues);
                        rowView.findViewById(R.id.txtInvite).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PublicLeagues temp = (PublicLeagues) v.getTag();
                                ((TextView) v).setText("INVITE");
                                mLeagueId = temp.getId() + "";
                                matchId = temp.getMatch_id() + "";
                                hitApiRequest(ApiConstants.REQUEST_JOIN_LEAGUE, true);
                            }
                        });
                    }

                }

                linearLayout.addView(rowView);
            }
        }
    }

    private void addPublicLeagueList(final Matches match) {
        List<LeaguesCategories> leaguesCategories = match.getLeaguesCategoriesList();
        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.lnrPublicLeague);
        for (int i = 0; i <= leaguesCategories.size() - 1; i++) {
            for (int j = 0; j <= leaguesCategories.get(i).getLeagues().size() - 1; j++) {
                PublicLeagues publicLeagues = leaguesCategories.get(i).getLeagues().get(j);
                if (publicLeagues.isMega() == false) {
                    View rowView = mInflater.inflate(R.layout.list_item_league_selection, null);
                    ((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.getWin() + "");
                    ((TextView) rowView.findViewById(R.id.txtMembers)).setText(publicLeagues.getFilled_seats() + "/" + publicLeagues.getTotal_seats());
                    ((TextView) rowView.findViewById(R.id.txtPaid)).setText(publicLeagues.getFee() + "");
                    //((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.get(i).getWin() + "");

                    if (publicLeagues.getTotal_seats() == publicLeagues.getFilled_seats()) {
                        ((TextView) rowView.findViewById(R.id.txtInvite)).setText("COMPLETE");
                    } else {
                        if (publicLeagues.isJoined()) {
                            ((TextView) rowView.findViewById(R.id.txtInvite)).setText("INVITE");
                            rowView.findViewById(R.id.txtInvite).setTag(publicLeagues);
                            rowView.findViewById(R.id.txtInvite).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    PublicLeagues temp = (PublicLeagues) v.getTag();
                                    mLeagueId = temp.getId() + "";
                                    matchId = temp.getMatch_id() + "";
                                    hitApiRequest(ApiConstants.REQUEST_INVITE_CODE, true);
                                }
                            });
                        } else {
                            ((TextView) rowView.findViewById(R.id.txtInvite)).setText("JOIN");
                            rowView.findViewById(R.id.txtInvite).setTag(publicLeagues);
                            rowView.findViewById(R.id.txtInvite).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    PublicLeagues temp = (PublicLeagues) v.getTag();
                                    ((TextView) v).setText("INVITE");
                                    mLeagueId = temp.getId() + "";
                                    matchId = temp.getMatch_id() + "";
                                    hitApiRequest(ApiConstants.REQUEST_JOIN_LEAGUE, true);
                                }
                            });
                        }

                    }

                    linearLayout.addView(rowView);
                }
            }

        }
    }
}
