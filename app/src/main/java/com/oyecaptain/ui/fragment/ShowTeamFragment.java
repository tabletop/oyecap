package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.Players;
import com.oyecaptain.model.response.PlayersResponse;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 24/05/16.
 */
public class ShowTeamFragment extends BaseFragment {


    private String TAG = ShowTeamFragment.class.getSimpleName();
    private View mView;
    private String matchId;
    private String tournamentId;
    private Matches matches;
    private String url;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_show_team, null);


        Intent intent = getActivity().getIntent();
        if (intent != null) {
            matchId = intent.getStringExtra(AppConstants.EXTRA_MATCH_ID);
            tournamentId = intent.getStringExtra(AppConstants.EXTRA_TOURNAMENT_ID);
            matches = intent.getParcelableExtra(AppConstants.EXTRA_MATCH);
            hitApiRequest(ApiConstants.REQUEST_GET_PLAYERS, true);
        }

        mView.findViewById(R.id.txtEditTeam).setOnClickListener(this);


        return mView;
    }


    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_PLAYERS:
                    url = String.format(ApiConstants.URL_PLAYERS, matchId, tournamentId);
                    className = PlayersResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_PLAYERS) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_PLAYERS:
                    PlayersResponse playersResponse = (PlayersResponse) responseObject;
                    if (playersResponse != null && playersResponse.getPlayers() != null) {
                        updatePlayers(playersResponse.getPlayers());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void updatePlayers(List<Players> players) {
        int i = 3;
        for (Players temp : players) {
            if (temp.isSelected() == true) {
                if (temp.getP_type().equalsIgnoreCase("c")) {
                    updateCaptain(temp);
                } else if (temp.getP_type().equalsIgnoreCase("v")) {
                    updateViceCaptain(temp);
                } else {
                    if (i > 12) {
                        break;
                    }
                    updatePlayer(temp, i);
                    i++;
                }
            }
        }

    }

    private void updateCaptain(Players players) {
        View captainView = mView.findViewById(R.id.player1);
        ((TextView) captainView.findViewById(R.id.txtName)).setText(players.getName());
        ((TextView) captainView.findViewById(R.id.txtPlayerType)).setText("C");
        ((TextView) captainView.findViewById(R.id.txtPlayerType)).setVisibility(View.VISIBLE);
        ((TextView) captainView.findViewById(R.id.txtPoint)).setText(players.getPoints() + "");

        updatePlayerTypeImage(players, captainView);

        if (matches != null && matches.getTeams() != null && matches.getTeams().size() > 1) {
            if (matches.getTeams().get(0).getId() == players.getTeam()) {
                ((TextView) captainView.findViewById(R.id.txtName)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.jersey_blue, 0, 0);
            } else {
                ((TextView) captainView.findViewById(R.id.txtName)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.jersey_green, 0, 0);
            }
        }
    }

    private void updateViceCaptain(Players players) {
        View viceCaptainView = mView.findViewById(R.id.player2);
        ((TextView) viceCaptainView.findViewById(R.id.txtName)).setText(players.getName());
        ((TextView) viceCaptainView.findViewById(R.id.txtPlayerType)).setText("VC");
        ((TextView) viceCaptainView.findViewById(R.id.txtPlayerType)).setVisibility(View.VISIBLE);
        ((TextView) viceCaptainView.findViewById(R.id.txtPoint)).setText(players.getPoints() + "");

        updatePlayerTypeImage(players, viceCaptainView);

        if (matches != null && matches.getTeams() != null && matches.getTeams().size() > 1) {
            if (matches.getTeams().get(0).getId() == players.getTeam()) {
                ((TextView) viceCaptainView.findViewById(R.id.txtName)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.jersey_blue, 0, 0);
            } else {
                ((TextView) viceCaptainView.findViewById(R.id.txtName)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.jersey_green, 0, 0);
            }
        }
    }


    private void updatePlayer(Players players, int i) {
        int id = getResources().getIdentifier("player" + i, "id", getActivity().getPackageName());
        View playerView = mView.findViewById(id);
        ((TextView) playerView.findViewById(R.id.txtName)).setText(players.getName());
        //((TextView) playerView.findViewById(R.id.txtPlayerType)).setText("P");
        ((TextView) playerView.findViewById(R.id.txtPlayerType)).setVisibility(View.GONE);
        ((TextView) playerView.findViewById(R.id.txtPoint)).setText(players.getPoints() + "");

        updatePlayerTypeImage(players, playerView);

        if (matches != null && matches.getTeams() != null && matches.getTeams().size() > 1) {
            if (matches.getTeams().get(0).getId() == players.getTeam()) {
                ((TextView) playerView.findViewById(R.id.txtName)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.jersey_blue, 0, 0);
            } else {
                ((TextView) playerView.findViewById(R.id.txtName)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.jersey_green, 0, 0);
            }
        }
    }

    private void updatePlayerTypeImage(Players players, View viceCaptainView) {
        //1 for Bateman
        //2 for bowler
        //3 for wicketkeeper
        //4 for all rounder

        if (players.getType() == 1) {
            ((ImageView) viceCaptainView.findViewById(R.id.imgPlayerType)).setImageResource(R.drawable.batsmen_white);
        } else if (players.getType() == 2) {
            ((ImageView) viceCaptainView.findViewById(R.id.imgPlayerType)).setImageResource(R.drawable.bowler_white);
        } else if (players.getType() == 3) {
            ((ImageView) viceCaptainView.findViewById(R.id.imgPlayerType)).setImageResource(R.drawable.wicketkeeper_white);
        } else if (players.getType() == 4) {
            ((ImageView) viceCaptainView.findViewById(R.id.imgPlayerType)).setImageResource(R.drawable.allrounder_white);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtEditTeam:
                break;
            default:
                super.onClick(view);
        }

    }
}
