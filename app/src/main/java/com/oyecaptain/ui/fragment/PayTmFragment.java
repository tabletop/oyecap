package com.oyecaptain.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.utils.OyeCapPrefernece;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daemonn on 24/05/16.
 */
public class PayTmFragment extends BaseFragment {


    private String TAG = PayTmFragment.class.getSimpleName();
    private View mView;
    private EditText edtAmount;
    TextView addFunds;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_paytm, null);

        edtAmount = (EditText) mView.findViewById(R.id.edtAmount);
        edtAmount.setOnClickListener(this);
        addFunds = (TextView) mView.findViewById(R.id.addFunds);
        addFunds.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addFunds:
                if (validate()) {
                    payViPatTM();
                }
                break;
            default:
                super.onClick(view);
        }
    }

    public void payViPatTM() {
        PaytmPGService service = PaytmPGService.getProductionService();

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("REQUEST_TYPE", "DEFAULT");
        paramMap.put("ORDER_ID", "" + System.currentTimeMillis());
        paramMap.put("MID", "MirchE05412106737599");
        paramMap.put("CUST_ID", OyeCapPrefernece.getInstance().getUserId());
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail120");
        paramMap.put("WEBSITE", "MirchEntertWAP");
        paramMap.put("TXN_AMOUNT", edtAmount.getText().toString());
        paramMap.put("THEME", "merchant");

        String success = ApiConstants.URL_GENERATE_CHECKSUM;
        String fail = ApiConstants.URL_VERIFY_CHECKSUM;
        PaytmMerchant Merchant = new PaytmMerchant(success, fail);


        PaytmOrder paytmOrder = new PaytmOrder(paramMap);
        service.initialize(paytmOrder, Merchant, null);

        service.startPaymentTransaction(getActivity(), true, true, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionSuccess(Bundle bundle) {
                Log.d(TAG, "onTransactionSuccess");
            }

            @Override
            public void onTransactionFailure(String s, Bundle bundle) {
                Log.d(TAG, "onTransactionFailure");
                // ToastUtils.showToast(getActivity(),"Transaction Failed");
            }

            @Override
            public void networkNotAvailable() {
                Log.d(TAG, "networkNotAvailable");
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                Log.d(TAG, "clientAuthenticationFailed");
            }

            @Override
            public void someUIErrorOccurred(String s) {
                Log.d(TAG, "someUIErrorOccurred");
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
                Log.d(TAG, "onErrorLoadingWebPage");
            }

            @Override
            public void onBackPressedCancelTransaction() {
                Log.d(TAG, "onBackPressedCancelTransaction");
            }
        });

    }

    private boolean validate() {
        if (StringUtils.isNullOrEmpty(edtAmount.getText().toString().trim())) {
            ToastUtils.showToast(getActivity(), "Please enter amount");
            return false;
        }
        return true;
    }

}
