package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.response.InviteCodeResponse;
import com.oyecaptain.model.response.JoinedLeagueResponse;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.PublicLeagues;
import com.oyecaptain.ui.activity.JoinedLeagueActivity;
import com.oyecaptain.ui.activity.LeagueSelectionActivity;
import com.oyecaptain.ui.adapter.LiveGamesRecyclerAdapter;
import com.oyecaptain.utils.ShareIntentUtil;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class JoinedLeagueFragment extends BaseFragment {

    private String TAG = JoinedLeagueFragment.class.getSimpleName();
    private View mView;
    private String url;
    private String mTournamentId;
    private RecyclerView listview;
    private LiveGamesRecyclerAdapter liveGamesRecyclerAdapter;
    private String matchId;
    private String matchname;
    private LayoutInflater mInflater;
    private String mLeagueId;
    private boolean showJoinMoreLeague;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_joined_league, null);
        mInflater = LayoutInflater.from(getActivity().getBaseContext());

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            matchId = intent.getStringExtra(AppConstants.EXTRA_MATCH_ID);
            matchname = intent.getStringExtra(AppConstants.EXTRA_MATCH_NAME);
            showJoinMoreLeague = intent.getBooleanExtra(AppConstants.EXTRA_SHOW_JOIN_MORE_LEAGUE, false);
        }

        if (showJoinMoreLeague) {
            mView.findViewById(R.id.txtJoinMore).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.txtJoinMore).setVisibility(View.GONE);
        }


        hitApiRequest(ApiConstants.REQUEST_GET_JOINED_LEAGUES, true);
        mView.findViewById(R.id.txtJoinMore).setOnClickListener(this);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_JOINED_LEAGUES:
                    url = String.format(ApiConstants.URL_JOINED_LEAGUES, matchId);
                    className = JoinedLeagueResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                case ApiConstants.REQUEST_INVITE_CODE:
                    ((BaseActivity) getActivity()).showProgressDialog();
                    url = String.format(ApiConstants.URL_INVITE_CODE, mLeagueId);
                    className = InviteCodeResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_JOINED_LEAGUES) {
        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_JOINED_LEAGUES:
                    JoinedLeagueResponse joinedLeagueResponse = (JoinedLeagueResponse) responseObject;
                    if (joinedLeagueResponse != null && joinedLeagueResponse.getPublicLeagues() != null) {
                        addPublicLeagueList(joinedLeagueResponse.getPublicLeagues());
                        addPrivateLeagueList(joinedLeagueResponse.getPrivateLeagues());
                    }
                    break;
                case ApiConstants.REQUEST_INVITE_CODE:
                    InviteCodeResponse inviteCodeResponse = (InviteCodeResponse) responseObject;
                    if (inviteCodeResponse != null) {
                        ShareIntentUtil.shareEvent(getActivity(), ApiConstants.URL_WEB + inviteCodeResponse.getUrl());
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void addPrivateLeagueList(List<PublicLeagues> privateLeagues) {
        if (privateLeagues.size() > 0) {
            mView.findViewById(R.id.txtNoDataPrivate).setVisibility(View.GONE);
        } else {
            mView.findViewById(R.id.txtNoDataPrivate).setVisibility(View.VISIBLE);
        }
        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.lnrPrivateLeague);
        for (int i = 0; i <= privateLeagues.size() - 1; i++) {
            View rowView = mInflater.inflate(R.layout.list_item_league_selection, null);
            ((TextView) rowView.findViewById(R.id.txtWin)).setText(privateLeagues.get(i).getWin() + "");
            ((TextView) rowView.findViewById(R.id.txtMembers)).setText(privateLeagues.get(i).getFilled_seats() + "/" + privateLeagues.get(i).getTotal_seats());
            ((TextView) rowView.findViewById(R.id.txtPaid)).setText(privateLeagues.get(i).getFee() + "");
            //((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.get(i).getWin() + "");

            if (privateLeagues.get(i).getTotal_seats() == privateLeagues.get(i).getFilled_seats()) {
                ((TextView) rowView.findViewById(R.id.txtInvite)).setText("COMPLETE");
            } else {
                rowView.findViewById(R.id.txtInvite).setTag(privateLeagues.get(i));
                rowView.findViewById(R.id.txtInvite).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PublicLeagues temp = (PublicLeagues) v.getTag();
                        mLeagueId = temp.getId() + "";
                        hitApiRequest(ApiConstants.REQUEST_INVITE_CODE, true);

                    }
                });
            }

            linearLayout.addView(rowView);
        }
    }

    private void addPublicLeagueList(List<PublicLeagues> publicLeagues) {
        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.lnrPublicLeague);
        if (publicLeagues.size() > 0) {
            mView.findViewById(R.id.txtNoDataPublic).setVisibility(View.GONE);
        } else {
            mView.findViewById(R.id.txtNoDataPublic).setVisibility(View.VISIBLE);
        }
        for (int i = 0; i <= publicLeagues.size() - 1; i++) {
            View rowView = mInflater.inflate(R.layout.list_item_league_selection, null);
            ((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.get(i).getWin() + "");
            ((TextView) rowView.findViewById(R.id.txtMembers)).setText(publicLeagues.get(i).getFilled_seats() + "/" + publicLeagues.get(i).getTotal_seats());
            ((TextView) rowView.findViewById(R.id.txtPaid)).setText(publicLeagues.get(i).getFee() + "");
            if (publicLeagues.get(i).getFilled_seats() == publicLeagues.get(i).getTotal_seats()) {
                ((TextView) rowView.findViewById(R.id.txtInvite)).setText("COMPLETE");
            } else {
                rowView.findViewById(R.id.txtInvite).setTag(publicLeagues.get(i));
                rowView.findViewById(R.id.txtInvite).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PublicLeagues temp = (PublicLeagues) v.getTag();
                        mLeagueId = temp.getId() + "";
                        hitApiRequest(ApiConstants.REQUEST_INVITE_CODE, true);

                    }
                });
            }
            //((TextView) rowView.findViewById(R.id.txtWin)).setText(publicLeagues.get(i).getWin() + "");
            linearLayout.addView(rowView);
        }
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtJoinMore:
                intent = new Intent(getActivity(), LeagueSelectionActivity.class);
                intent.putExtra(AppConstants.EXTRA_MATCH_ID, matchId + "");
                intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matchname);
                startActivity(intent);
                break;
            default:
                super.onClick(view);
        }

    }

}
