package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.oyecaptain.R;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.ui.activity.PaymentMethodActivity;

/**
 * Created by daemonn on 24/05/16.
 */
public class AddCashFragment extends BaseFragment {


    private String TAG = AddCashFragment.class.getSimpleName();
    private View mView;
    private EditText edtOther;
    private int amount = 100;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_add_cash, null);

        Toolbar toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        toolbar.setTitle("ADD CASH");
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edtOther = (EditText) mView.findViewById(R.id.edtOther);

        mView.findViewById(R.id.layout_100).setOnClickListener(this);
        mView.findViewById(R.id.layout_500).setOnClickListener(this);
        mView.findViewById(R.id.layout_1000).setOnClickListener(this);
        mView.findViewById(R.id.layout_coupon).setOnClickListener(this);
        mView.findViewById(R.id.addFunds).setOnClickListener(this);


        return mView;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.addFunds:
                intent = new Intent(getActivity(), PaymentMethodActivity.class);
                intent.putExtra(AppConstants.EXTRA_AMOUNT, amount);
                intent.putExtra(AppConstants.EXTRA_COUPON_CODE, edtOther.getText().toString());
                startActivity(intent);
                break;
            case R.id.layout_100:
                break;
            case R.id.layout_coupon:
                intent = new Intent(getActivity(), PaymentMethodActivity.class);
                intent.putExtra(AppConstants.EXTRA_AMOUNT, amount);
                intent.putExtra(AppConstants.EXTRA_COUPON_CODE, edtOther.getText().toString());
                startActivity(intent);
                break;
        }
        super.onClick(view);
    }
}
