package com.oyecaptain.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.constants.AppConstants;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.Players;
import com.oyecaptain.model.response.PlayersResponse;
import com.oyecaptain.ui.activity.CaptainViceCaptainActivity;
import com.oyecaptain.ui.activity.LeagueSelectionActivity;
import com.oyecaptain.ui.adapter.PlayerRecyclerAdapter;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daemonn on 24/05/16.
 */
public class SelectTeamFragment extends BaseFragment {


    private String TAG = SelectTeamFragment.class.getSimpleName();
    private Handler timerHandler = new Handler();
    private View mView;
    private String url;
    private String matchId;
    private String matchName;
    private String tournamentId;
    private Matches matches;
    private RecyclerView listview;
    private List<Players> mPlayers;
    private PlayerRecyclerAdapter playerRecyclerAdapter;
    private int teamId1;
    private int teamId2;
    private int wkCount = 0;
    private int bowlerCount = 0;
    private int arCount = 0;
    private int batsmanCount = 0;
    private int team1Count = 0;
    private int team2Count = 0;
    private boolean isTeamEditable;
    private float creditConsumed;


    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            ((TextView) mView.findViewById(R.id.txtTimeDifference)).setText(DateUtils.getTimerText(matches.getFormat_time()));
            timerHandler.postDelayed(this, 1000);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_select_team, null);


        Intent intent = getActivity().getIntent();
        if (intent != null) {
            matchId = intent.getStringExtra(AppConstants.EXTRA_MATCH_ID);
            matchName = intent.getStringExtra(AppConstants.EXTRA_MATCH_NAME);
            tournamentId = intent.getStringExtra(AppConstants.EXTRA_TOURNAMENT_ID);
            matches = intent.getParcelableExtra(AppConstants.EXTRA_MATCH);
            isTeamEditable = intent.getBooleanExtra(AppConstants.EXTRA_TEAM_EDITABLE, false);
            updateMatchToUI(matches);
            hitApiRequest(ApiConstants.REQUEST_GET_PLAYERS, true);
        }

        listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.addItemDecoration(new VerticalItemDecoration((int) (20 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);
        playerRecyclerAdapter = new PlayerRecyclerAdapter(getActivity(), this);
        listview.setAdapter(playerRecyclerAdapter);
        playerRecyclerAdapter.setMatches(matches);


        mView.findViewById(R.id.rtlWK).setOnClickListener(this);
        mView.findViewById(R.id.rtlBatsman).setOnClickListener(this);
        mView.findViewById(R.id.rtlAR).setOnClickListener(this);
        mView.findViewById(R.id.rtlBowler).setOnClickListener(this);
        mView.findViewById(R.id.txtChangeMatch).setOnClickListener(this);
        mView.findViewById(R.id.txtSelectcaptain).setOnClickListener(this);

        return mView;
    }

    private void updateMatchToUI(Matches matches) {
        if (matches.getTeams() != null && matches.getTeams().size() > 1) {
            teamId1 = matches.getTeams().get(0).getId();
            teamId2 = matches.getTeams().get(1).getId();
        } else {
            return;
        }

        ((TextView) mView.findViewById(R.id.txtTeam1)).setText(matches.getTeams().get(0).getName());
        ((TextView) mView.findViewById(R.id.txtTeam2)).setText(matches.getTeams().get(1).getName());


        if (!StringUtils.isNullOrEmpty(matches.getTeams().get(0).getImg_url())) {
            ((BaseActivity) getActivity()).loadParseFileInBackground(matches.getTeams().get(0).getImg_url(), ((ImageView) mView.findViewById(R.id.imgTeam1)), -1);
            ((TextView) mView.findViewById(R.id.txtTeamCode1)).setVisibility(View.GONE);
            mView.findViewById(R.id.imgTeam1).setVisibility(View.VISIBLE);
        } else {
            ((TextView) mView.findViewById(R.id.txtTeamCode1)).setText(matches.getTeams().get(0).getTeam_code());
            ((TextView) mView.findViewById(R.id.txtTeamCode1)).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.imgTeam1).setVisibility(View.GONE);
        }

        if (!StringUtils.isNullOrEmpty(matches.getTeams().get(1).getImg_url())) {
            ((BaseActivity) getActivity()).loadParseFileInBackground(matches.getTeams().get(1).getImg_url(), ((ImageView) mView.findViewById(R.id.imgTeam2)), -1);
            ((TextView) mView.findViewById(R.id.txtTeamCode2)).setVisibility(View.GONE);
            mView.findViewById(R.id.imgTeam2).setVisibility(View.VISIBLE);
        } else {
            ((TextView) mView.findViewById(R.id.txtTeamCode2)).setText(matches.getTeams().get(1).getTeam_code());
            ((TextView) mView.findViewById(R.id.txtTeamCode2)).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.imgTeam2).setVisibility(View.GONE);
        }

        ((TextView) mView.findViewById(R.id.txtTimeDifference)).setText(DateUtils.getTimerText(matches.getFormat_time()));
        timerHandler.postDelayed(timerRunnable, 1000);

    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_PLAYERS:
                    url = String.format(ApiConstants.URL_PLAYERS, matchId, tournamentId);
                    className = PlayersResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_PLAYERS) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_PLAYERS:
                    PlayersResponse playersResponse = (PlayersResponse) responseObject;
                    if (playersResponse != null && playersResponse.getPlayers() != null) {
                        if (playersResponse.isSelected() == true && isTeamEditable == false) {
                            Intent intent = new Intent(getActivity(), LeagueSelectionActivity.class);
                            intent.putExtra(AppConstants.EXTRA_MATCH_ID, matchId + "");
                            intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matchName);
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            mPlayers = playersResponse.getPlayers();
                            setPlayersAdapter(playersResponse.getPlayers());
                            getPlayerAndTeamCount();
                            updatePlayesrsCount();
                            mView.findViewById(R.id.rtlWK).performClick();
                        }
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setPlayersAdapter(List<Players> players) {
        playerRecyclerAdapter.setListData(players);
        playerRecyclerAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rtlWK:
                ((TextView) mView.findViewById(R.id.chkAR)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkBatsman)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkBowlers)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkWK)).setSelected(true);
                ((TextView) mView.findViewById(R.id.txtSelectionCount)).setText("Select 1 Wicket Kepper");
                setWicketKeepers();
                break;
            case R.id.rtlBatsman:
                ((TextView) mView.findViewById(R.id.chkAR)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkBatsman)).setSelected(true);
                ((TextView) mView.findViewById(R.id.chkBowlers)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkWK)).setSelected(false);
                ((TextView) mView.findViewById(R.id.txtSelectionCount)).setText("Select 3 to 5 Batsman");
                setBatsman();
                break;
            case R.id.rtlBowler:
                ((TextView) mView.findViewById(R.id.chkAR)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkBatsman)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkBowlers)).setSelected(true);
                ((TextView) mView.findViewById(R.id.chkWK)).setSelected(false);
                ((TextView) mView.findViewById(R.id.txtSelectionCount)).setText("Select 3 to 5 Bowler");
                setBowler();
                break;
            case R.id.rtlAR:
                ((TextView) mView.findViewById(R.id.chkAR)).setSelected(true);
                ((TextView) mView.findViewById(R.id.chkBatsman)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkBowlers)).setSelected(false);
                ((TextView) mView.findViewById(R.id.chkWK)).setSelected(false);
                ((TextView) mView.findViewById(R.id.txtSelectionCount)).setText("Select 1 to 3 All Rounder");
                setAllRounder();
                break;
            case R.id.txtSelectcaptain:
                if (validdateTeamMinPlayers()) {
                    if ((arCount + batsmanCount + bowlerCount + wkCount) == 11) {
                        List<Players> selectedPlayersList = getSeletedPlayers();
                        Intent intent = new Intent(getActivity(), CaptainViceCaptainActivity.class);
                        intent.putExtra(AppConstants.EXTRA_MATCH_ID, matchId + "");
                        intent.putExtra(AppConstants.EXTRA_TOURNAMENT_ID, tournamentId + "");
                        intent.putExtra(AppConstants.EXTRA_MATCH_NAME, matchName);
                        intent.putParcelableArrayListExtra(AppConstants.EXTRA_PLAYERS, (ArrayList<? extends Parcelable>) selectedPlayersList);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.txtChangeMatch:
                getActivity().finish();
                break;
            case R.id.rtlparent:
                Players players = (Players) view.getTag();
                List<Players> playersList = playerRecyclerAdapter.getListData();
                if (playersList.contains(players)) {
                    int pos = playersList.indexOf(players);
                    if (playersList.get(pos).isSelected()) {
                        playersList.get(pos).setSelected(false);
                        getPlayerAndTeamCount();
                        updatePlayesrsCount();
                    } else {
                        getPlayerAndTeamCount();
                        if (playersList.get(pos).getType() == 1) {
                            if (validateBatsman(playersList.get(pos))) {
                                playersList.get(pos).setSelected(true);
                                batsmanCount++;
                            }
                        } else if (playersList.get(pos).getType() == 2) {
                            if (validdateBowler(playersList.get(pos))) {
                                playersList.get(pos).setSelected(true);
                                bowlerCount++;
                            }
                        } else if (playersList.get(pos).getType() == 3) {
                            if (vlaidateWK(playersList.get(pos))) {
                                playersList.get(pos).setSelected(true);
                                wkCount++;
                            }
                        } else if (playersList.get(pos).getType() == 4) {
                            if (validateAR(playersList.get(pos))) {
                                playersList.get(pos).setSelected(true);
                                arCount++;
                            }
                        }
                        updatePlayesrsCount();
                    }
                    playerRecyclerAdapter.notifyDataSetChanged();
                }
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validdateTeamMinPlayers() {
        getPlayerAndTeamCount();
        if (arCount < 1) {
            ToastUtils.showToast(getActivity(), "Select at least 1 All Rounder");
            return false;
        } else if (wkCount < 1) {
            ToastUtils.showToast(getActivity(), "Select at least 1 Wicket Keeper");
            return false;
        } else if (batsmanCount < 3) {
            ToastUtils.showToast(getActivity(), "Select at least 3 Batsman");
            return false;
        } else if (bowlerCount < 3) {
            ToastUtils.showToast(getActivity(), "Select at least 3 Bowler");
            return false;
        }
        return true;
    }


    private List<Players> getSeletedPlayers() {
        List<Players> selectedPlayers = new ArrayList<>();
        for (Players temp : mPlayers) {
            if (temp.isSelected() == true) {
                selectedPlayers.add(temp);
            }
        }
        return selectedPlayers;
    }

    private void updatePlayesrsCount() {
        ((TextView) mView.findViewById(R.id.txtWkCount)).setText(wkCount + "");
        ((TextView) mView.findViewById(R.id.txtArCount)).setText(arCount + "");
        ((TextView) mView.findViewById(R.id.txtBowlerCount)).setText(bowlerCount + "");
        ((TextView) mView.findViewById(R.id.txtBatsmanCount)).setText(batsmanCount + "");

        ((TextView) mView.findViewById(R.id.txtCredit)).setText("Credits Left : " + (100 - creditConsumed) + "");
        ((TextView) mView.findViewById(R.id.txtPlayersLeft)).setText("Players Left : " + (11 - (batsmanCount + wkCount + bowlerCount + arCount)) + "");

        if (batsmanCount > 0) {
            mView.findViewById(R.id.imgBatmanSel).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.imgBatmanSel).setVisibility(View.GONE);
        }

        if (wkCount > 0) {
            mView.findViewById(R.id.imgWKSel).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.imgWKSel).setVisibility(View.GONE);
        }
        if (arCount > 0) {
            mView.findViewById(R.id.imgARSel).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.imgARSel).setVisibility(View.GONE);
        }
        if (bowlerCount > 0) {
            mView.findViewById(R.id.imgBowlerSel).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.imgBowlerSel).setVisibility(View.GONE);
        }
    }


    private boolean validateAR(Players players) {
        if (validateTeamCount()) {
            if (players.getTeam() == teamId1) {
                if (validateTeam1()) {
                    if (arCount >= 3) {
                        ToastUtils.showToast(getActivity(), "All rounder max reached Count max reached");
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                if (validateTeam2()) {
                    if (arCount >= 3) {
                        ToastUtils.showToast(getActivity(), "All rounder max reached Count max reached");
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    private boolean vlaidateWK(Players players) {
        if (validateTeamCount()) {
            if (players.getTeam() == teamId1) {
                if (validateTeam1()) {
                    if (wkCount >= 1) {
                        ToastUtils.showToast(getActivity(), "Wicket keeper max count reached");
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                if (validateTeam2()) {
                    if (wkCount >= 1) {
                        ToastUtils.showToast(getActivity(), "Wicket keeper max count reached");
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    private boolean validdateBowler(Players players) {
        if (validateTeamCount()) {
            if (players.getTeam() == teamId1) {
                if (validateTeam1()) {
                    if (bowlerCount >= 5) {
                        ToastUtils.showToast(getActivity(), "Bowler Count max reached");
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                if (validateTeam2()) {
                    if (bowlerCount >= 5) {
                        ToastUtils.showToast(getActivity(), "Bowler Count max reached");
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    private boolean validateBatsman(Players players) {
        if (validateTeamCount()) {
            if (players.getTeam() == teamId1) {
                if (validateTeam1()) {
                    if (batsmanCount >= 5) {
                        ToastUtils.showToast(getActivity(), "Batsman max reached Count max reached");
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                if (validateTeam2()) {
                    if (batsmanCount >= 5) {
                        ToastUtils.showToast(getActivity(), "Batsman max reached Count max reached");
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }


    private boolean validateTeam2() {
        if (team2Count >= 7) {
            ToastUtils.showToast(getActivity(), "Team 1 has more than 7 menbers");
            return false;
        }

        return true;
    }

    private boolean validateTeam1() {
        if (team1Count >= 7) {
            ToastUtils.showToast(getActivity(), "Team 1 has more than 7 menbers");
            return false;
        }

        return true;
    }

    private boolean validateTeamCount() {
        if ((team1Count + team2Count) >= 11) {
            ToastUtils.showToast(getActivity(), "Team has already 11 players selected.");
            return false;
        }
        return true;
    }

    private void getPlayerAndTeamCount() {
        //1 for Bateman
        //2 for bowler
        //3 for wicketkeeper
        //4 for all rounder

        wkCount = 0;
        bowlerCount = 0;
        arCount = 0;
        batsmanCount = 0;
        team1Count = 0;
        team2Count = 0;
        creditConsumed = 0;

        List<Players> playersList = mPlayers;
        for (Players temp : playersList) {
            if (temp.getType() == 1 && temp.isSelected()) {
                batsmanCount++;
                creditConsumed = creditConsumed + temp.getCredit();
            } else if (temp.getType() == 2 && temp.isSelected()) {
                bowlerCount++;
                creditConsumed = creditConsumed + temp.getCredit();
            } else if (temp.getType() == 3 && temp.isSelected()) {
                wkCount++;
                creditConsumed = creditConsumed + temp.getCredit();
            } else if (temp.getType() == 4 && temp.isSelected()) {
                arCount++;
                creditConsumed = creditConsumed + temp.getCredit();
            }

            if (temp.getTeam() == teamId1 && temp.isSelected()) {
                team1Count++;
            } else if (temp.getTeam() == teamId2 && temp.isSelected()) {
                team2Count++;
            }
        }
    }


    private void setAllRounder() {
        //1 for Bateman
        //2 for bowler
        //3 for wicketkeeper
        //4 for all rounder

        List<Players> players = new ArrayList<>();
        for (Players temp : mPlayers) {
            if (temp.getType() == 4) {
                players.add(temp);
            }
        }
        setPlayersAdapter(players);
    }

    private void setBowler() {
        //1 for Bateman
        //2 for bowler
        //3 for wicketkeeper
        //4 for all rounder

        List<Players> players = new ArrayList<>();
        for (Players temp : mPlayers) {
            if (temp.getType() == 2) {
                players.add(temp);
            }
        }
        setPlayersAdapter(players);
    }

    private void setBatsman() {
        //1 for Bateman
        //2 for bowler
        //3 for wicketkeeper
        //4 for all rounder

        List<Players> players = new ArrayList<>();
        for (Players temp : mPlayers) {
            if (temp.getType() == 1) {
                players.add(temp);
            }
        }
        setPlayersAdapter(players);
    }

    private void setWicketKeepers() {
        //1 for Bateman
        //2 for bowler
        //3 for wicketkeeper
        //4 for all rounder

        List<Players> players = new ArrayList<>();
        for (Players temp : mPlayers) {
            if (temp.getType() == 3) {
                players.add(temp);
            }
        }
        setPlayersAdapter(players);
    }
}
