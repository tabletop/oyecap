package com.oyecaptain.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.oyecaptain.R;
import com.oyecaptain.constants.ApiConstants;
import com.oyecaptain.model.response.BalanceHistoryResponse;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.MatchesResponse;
import com.oyecaptain.model.response.TournamentResponse;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.model.response.Transactions;
import com.oyecaptain.ui.adapter.BalanceHistoryRecyclerAdapter;
import com.oyecaptain.ui.adapter.LiveGamesRecyclerAdapter;
import com.oyecaptain.utils.VerticalItemDecoration;

import org.json.JSONException;

import java.util.List;

/**
 * Created by daemonn on 16/04/16.
 */
public class BalanceHistoryFragment extends BaseFragment {

    private String TAG = BalanceHistoryFragment.class.getSimpleName();
    private View mView;
    private String url;
    private RecyclerView listview;
    private BalanceHistoryRecyclerAdapter balanceHistoryRecyclerAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_balance_history, null);


        listview = (RecyclerView) mView.findViewById(R.id.listview);
        listview.addItemDecoration(new VerticalItemDecoration((int) (10 * getResources().getDisplayMetrics().density)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);
        balanceHistoryRecyclerAdapter = new BalanceHistoryRecyclerAdapter(getActivity(), this);
        listview.setAdapter(balanceHistoryRecyclerAdapter);

        hitApiRequest(ApiConstants.REQUEST_GET_BALANCE_HISTORY, true);

        return mView;
    }

    public void hitApiRequest(int reqType, boolean showLoader) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        try {
            if (showLoader) {
                ((BaseActivity) getActivity()).showProgressDialog();
            }
            VolleyJsonRequest request;
            Class className;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_BALANCE_HISTORY:
                    url = ApiConstants.URL_BALANCE_HISTORY;
                    className = BalanceHistoryResponse.class;
                    request = VolleyJsonRequest.doget(url, new UpdateJsonListener(getActivity(), this, reqType, className) {
                    });
                    ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;
                default:
                    url = "";
                    className = null;

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected String getJsonString(int reqType) {
        if (reqType == ApiConstants.REQUEST_GET_BALANCE_HISTORY) {

        }
        return null;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_BALANCE_HISTORY:
                    BalanceHistoryResponse balanceHistoryResponse = (BalanceHistoryResponse) responseObject;
                    if (balanceHistoryResponse != null && balanceHistoryResponse.getTransactions() != null) {
                        setBalanceHistoryAdapter(balanceHistoryResponse.getTransactions());
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                super.onClick(view);
        }

    }


    public void setBalanceHistoryAdapter(List<Transactions> transactionsList) {
        balanceHistoryRecyclerAdapter.setListData(transactionsList);
        balanceHistoryRecyclerAdapter.notifyDataSetChanged();
    }
}
