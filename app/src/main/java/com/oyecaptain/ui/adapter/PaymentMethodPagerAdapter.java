package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.CouponCodeFragment;
import com.oyecaptain.ui.fragment.NetBankingFragment;
import com.oyecaptain.ui.fragment.PayTmFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class PaymentMethodPagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = PaymentMethodPagerAdapter.class.getSimpleName();
    private Context mContext;
    private NetBankingFragment netBankingFragment;
    private PayTmFragment payTmFragment;
    private CouponCodeFragment couponCodeFragment;

    public PaymentMethodPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (netBankingFragment == null) {
                netBankingFragment = new NetBankingFragment();
            }
            return netBankingFragment;
        } else if (position == 1) {
            if (payTmFragment == null) {
                payTmFragment = new PayTmFragment();
            }
            return payTmFragment;
        } else if (position == 2) {
            if (couponCodeFragment == null) {
                couponCodeFragment = new CouponCodeFragment();
            }
            return couponCodeFragment;
        }
        return netBankingFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return netBankingFragment;
        } else if (selectedTabPosition == 1) {
            return payTmFragment;
        } else if (selectedTabPosition == 2) {
            return couponCodeFragment;
        }

        return netBankingFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.net_banking);
        } else if (position == 1) {
            return mContext.getString(R.string.pay_tm);
        } else if (position == 2) {
            return mContext.getString(R.string.coupon_code);
        }
        return mContext.getString(R.string.net_banking);
    }

    @Override
    public int getCount() {
        return 3;
    }

}
