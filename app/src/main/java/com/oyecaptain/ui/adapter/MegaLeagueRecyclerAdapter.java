package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.krapps.utils.DateUtils;
import com.oyecaptain.R;
import com.oyecaptain.model.response.MegaLeagues;
import com.oyecaptain.ui.fragment.MegaLeaguesFragment;

import java.util.List;

public class MegaLeagueRecyclerAdapter extends RecyclerView.Adapter<MegaLeagueRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<MegaLeagues> mlistData;
    private OnClickListener mClickListener;
    private int pos;

    public MegaLeagueRecyclerAdapter(Context context, MegaLeaguesFragment megaLeaguesFragment) {
        mContext = context;
        mClickListener = megaLeaguesFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<MegaLeagues> megaLeagues) {
        this.mlistData = megaLeagues;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mega_league, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {


        viewholder.txtTournamentName.setText(mlistData.get(position).getTournament_name());
        viewholder.txtMatchName.setText(mlistData.get(position).getMatch_name());
        viewholder.txtEntryFees.setText("Entry Fee - " + "\u20B9" + mlistData.get(position).getFee());
        viewholder.txtPrize.setText("Prize - \u20B9" + mlistData.get(position).getWin());
        viewholder.txtTimeDifference.setText(DateUtils.getTimerText(mlistData.get(position).getFormat_time()));

        viewholder.txtJoinedMembers.setText("Joined - " + mlistData.get(position).getFilled_seats() + "/" + mlistData.get(position).getTotal_seats());

        if (mlistData.get(position).isJoined() == true) {
            viewholder.txtInvite.setText("INVITE");
        } else {
            viewholder.txtInvite.setText("JOIN & PLAY");
        }

        viewholder.rtlparent.setTag(mlistData.get(position));
        viewholder.txtInvite.setTag(mlistData.get(position));

        viewholder.rtlparent.setOnClickListener(mClickListener);
        viewholder.txtInvite.setOnClickListener(mClickListener);


    }

    public List<MegaLeagues> getListdata() {
        return mlistData;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtEntryFees;
        private final TextView txtInvite;
        private final TextView txtPrize;
        TextView txtTournamentName;
        TextView txtMatchName;
        TextView txtJoinedMembers;
        TextView txtTimeDifference;
        RelativeLayout rtlparent;

        public ViewHolder(View itemView) {
            super(itemView);

            rtlparent = (RelativeLayout) itemView.findViewById(R.id.rtlparent);
            txtTournamentName = (TextView) itemView.findViewById(R.id.txtTournamentName);
            txtMatchName = (TextView) itemView.findViewById(R.id.txtMatchName);
            txtTimeDifference = (TextView) itemView.findViewById(R.id.txtTimeDifference);
            txtJoinedMembers = (TextView) itemView.findViewById(R.id.txtJoinedMembers);
            txtEntryFees = (TextView) itemView.findViewById(R.id.txtEntryFees);
            txtInvite = (TextView) itemView.findViewById(R.id.txtInvite);
            txtPrize = (TextView) itemView.findViewById(R.id.txtPrize);
        }
    }


}
