package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.JoinedLeagueFragment;
import com.oyecaptain.ui.fragment.SelectTeamFragment;
import com.oyecaptain.ui.fragment.ShowTeamFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class JoinedLeaguePagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = JoinedLeaguePagerAdapter.class.getSimpleName();
    private Context mContext;
    private JoinedLeagueFragment joinedLeagueFragment;
    private ShowTeamFragment showTeamFragment;

    public JoinedLeaguePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (joinedLeagueFragment == null) {
                joinedLeagueFragment = new JoinedLeagueFragment();
            }
            return joinedLeagueFragment;
        } else if (position == 1) {
            if (showTeamFragment == null) {
                showTeamFragment = new ShowTeamFragment();
            }
            return showTeamFragment;
        }
        return joinedLeagueFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return joinedLeagueFragment;
        } else if (selectedTabPosition == 1) {
            return showTeamFragment;
        }

        return joinedLeagueFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.your_league);
        } else if (position == 1) {
            return mContext.getString(R.string.your_team);
        }
        return mContext.getString(R.string.your_league);
    }

    @Override
    public int getCount() {
        return 2;
    }

}
