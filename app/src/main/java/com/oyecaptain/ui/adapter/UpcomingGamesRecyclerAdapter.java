package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krapps.ui.BaseActivity;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.oyecaptain.R;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.ui.fragment.MatchesFragment;
import com.oyecaptain.ui.fragment.UpcomingGamesFragment;

import java.util.List;

public class UpcomingGamesRecyclerAdapter extends RecyclerView.Adapter<UpcomingGamesRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Matches> mlistData;
    private OnClickListener mClickListener;
    private int pos;

    public UpcomingGamesRecyclerAdapter(Context context, UpcomingGamesFragment upcomingGamesFragment) {
        mContext = context;
        mClickListener = upcomingGamesFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Matches> matches) {
        this.mlistData = matches;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_upcoming_games, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {


        viewholder.txtName.setText(mlistData.get(position).getName());
        viewholder.txtTeam1.setText(mlistData.get(position).getTeams().get(0).getName());
        viewholder.txtTeam2.setText(mlistData.get(position).getTeams().get(1).getName());

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getTeams().get(0).getImg_url())) {
            ((BaseActivity) mContext).loadParseFileInBackground(mlistData.get(position).getTeams().get(0).getImg_url(), viewholder.imgTeam1, -1);
            viewholder.txtTeamCode1.setVisibility(View.GONE);
            viewholder.imgTeam1.setVisibility(View.VISIBLE);
        } else {
            viewholder.txtTeamCode1.setText(mlistData.get(position).getTeams().get(0).getTeam_code());
            viewholder.txtTeamCode1.setVisibility(View.VISIBLE);
            viewholder.imgTeam1.setVisibility(View.GONE);
        }

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getTeams().get(1).getImg_url())) {
            ((BaseActivity) mContext).loadParseFileInBackground(mlistData.get(position).getTeams().get(1).getImg_url(), viewholder.imgTeam2, -1);
            viewholder.txtTeamCode2.setVisibility(View.GONE);
            viewholder.imgTeam2.setVisibility(View.VISIBLE);
        } else {
            viewholder.txtTeamCode2.setText(mlistData.get(position).getTeams().get(1).getTeam_code());
            viewholder.txtTeamCode2.setVisibility(View.VISIBLE);
            viewholder.imgTeam2.setVisibility(View.GONE);
        }

        viewholder.txtTimeDifference.setText(DateUtils.getTimerText(mlistData.get(position).getFormat_time()));

        viewholder.txtJoinMoreLeague.setTag(mlistData.get(position));
        viewholder.lnrTeams.setTag(mlistData.get(position));
        viewholder.txtEditTeam.setTag(mlistData.get(position));


        viewholder.txtJoinMoreLeague.setOnClickListener(mClickListener);
        viewholder.lnrTeams.setOnClickListener(mClickListener);
        viewholder.txtEditTeam.setOnClickListener(mClickListener);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout lnrTeams;
        private final TextView txtName;
        TextView txtEditTeam;
        TextView txtTeam1, txtTeamCode1;
        TextView txtTeam2, txtTeamCode2;

        ImageView imgTeam1;
        ImageView imgTeam2;
        TextView txtJoinMoreLeague;
        TextView txtTimeDifference;

        public ViewHolder(View itemView) {
            super(itemView);

            txtName = (TextView) itemView.findViewById(R.id.txtName);
            lnrTeams = (LinearLayout) itemView.findViewById(R.id.lnrTeams);
            txtTeam1 = (TextView) itemView.findViewById(R.id.txtTeam1);
            txtTeam2 = (TextView) itemView.findViewById(R.id.txtTeam2);
            txtTeamCode1 = (TextView) itemView.findViewById(R.id.txtTeamCode1);
            txtTeamCode2 = (TextView) itemView.findViewById(R.id.txtTeamCode2);
            txtTimeDifference = (TextView) itemView.findViewById(R.id.txtTimeDifference);
            txtJoinMoreLeague = (TextView) itemView.findViewById(R.id.txtJoinMoreLeague);
            txtEditTeam = (TextView) itemView.findViewById(R.id.txtEditTeam);
            imgTeam1 = (ImageView) itemView.findViewById(R.id.imgTeam1);
            imgTeam2 = (ImageView) itemView.findViewById(R.id.imgTeam2);
        }
    }


}
