package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krapps.ui.BaseActivity;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.oyecaptain.R;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.ui.fragment.LiveGamesFragment;
import com.oyecaptain.ui.fragment.MatchesFragment;
import com.oyecaptain.ui.fragment.MyContestFragment;
import com.oyecaptain.ui.fragment.PastGamesFragment;

import java.util.List;

public class LiveGamesRecyclerAdapter extends RecyclerView.Adapter<LiveGamesRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Matches> mlistData;
    private OnClickListener mClickListener;
    private int pos;

    public LiveGamesRecyclerAdapter(Context context, LiveGamesFragment liveGamesFragment) {
        mContext = context;
        mClickListener = liveGamesFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public LiveGamesRecyclerAdapter(Context context, PastGamesFragment pastGamesFragment) {
        mContext = context;
        mClickListener = pastGamesFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Matches> matches) {
        this.mlistData = matches;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_live_games, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {


        viewholder.txtName.setText(mlistData.get(position).getName());
        viewholder.txtTeam1.setText(mlistData.get(position).getTeams().get(0).getName());
        viewholder.txtTeam2.setText(mlistData.get(position).getTeams().get(1).getName());


        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getTeams().get(0).getImg_url())) {
            ((BaseActivity) mContext).loadParseFileInBackground(mlistData.get(position).getTeams().get(0).getImg_url(), viewholder.imgTeam1, -1);
            viewholder.txtTeamCode1.setVisibility(View.GONE);
            viewholder.imgTeam1.setVisibility(View.VISIBLE);
        } else {
            viewholder.txtTeamCode1.setText(mlistData.get(position).getTeams().get(0).getTeam_code());
            viewholder.txtTeamCode1.setVisibility(View.VISIBLE);
            viewholder.imgTeam1.setVisibility(View.GONE);
        }

        if (!StringUtils.isNullOrEmpty(mlistData.get(position).getTeams().get(1).getImg_url())) {
            ((BaseActivity) mContext).loadParseFileInBackground(mlistData.get(position).getTeams().get(1).getImg_url(), viewholder.imgTeam2, -1);
            viewholder.txtTeamCode2.setVisibility(View.GONE);
            viewholder.imgTeam2.setVisibility(View.VISIBLE);
        } else {
            viewholder.txtTeamCode2.setText(mlistData.get(position).getTeams().get(1).getTeam_code());
            viewholder.txtTeamCode2.setVisibility(View.VISIBLE);
            viewholder.imgTeam2.setVisibility(View.GONE);
        }

        if (mClickListener instanceof LiveGamesFragment) {
            viewholder.txtTimeDifference.setText((int) mlistData.get(position).getTotal_score() + " PTS");
        } else if (mClickListener instanceof PastGamesFragment) {
            if (mlistData.get(position).getWon() == 0) {
                viewholder.txtTimeDifference.setText("LOST");
            } else {
                viewholder.txtTimeDifference.setText("WON");
            }
        }


        viewholder.txtPointCard.setTag(mlistData.get(position));
        viewholder.txtScoreCard.setTag(mlistData.get(position));
        viewholder.lnrTeams.setTag(mlistData.get(position));

        viewholder.txtPointCard.setOnClickListener(mClickListener);
        viewholder.txtScoreCard.setOnClickListener(mClickListener);
        viewholder.lnrTeams.setOnClickListener(mClickListener);


    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtScoreCard;
        TextView txtTeam1, txtTeamCode1;
        TextView txtTeam2, txtTeamCode2;

        ImageView imgTeam1;
        ImageView imgTeam2;
        TextView txtPointCard;
        TextView txtTimeDifference;
        TextView txtName;
        LinearLayout lnrTeams;

        public ViewHolder(View itemView) {
            super(itemView);

            lnrTeams = (LinearLayout) itemView.findViewById(R.id.lnrTeams);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtTeam1 = (TextView) itemView.findViewById(R.id.txtTeam1);
            txtTeam2 = (TextView) itemView.findViewById(R.id.txtTeam2);
            txtTeamCode1 = (TextView) itemView.findViewById(R.id.txtTeamCode1);
            txtTeamCode2 = (TextView) itemView.findViewById(R.id.txtTeamCode2);
            txtTimeDifference = (TextView) itemView.findViewById(R.id.txtTimeDifference);
            txtPointCard = (TextView) itemView.findViewById(R.id.txtPointCard);
            txtScoreCard = (TextView) itemView.findViewById(R.id.txtScoreCard);
            imgTeam1 = (ImageView) itemView.findViewById(R.id.imgTeam1);
            imgTeam2 = (ImageView) itemView.findViewById(R.id.imgTeam2);
        }
    }


}
