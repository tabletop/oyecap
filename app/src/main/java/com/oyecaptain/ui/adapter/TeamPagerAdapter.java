package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.BalanceFragment;
import com.oyecaptain.ui.fragment.BalanceHistoryFragment;
import com.oyecaptain.ui.fragment.SelectTeamFragment;
import com.oyecaptain.ui.fragment.ShowTeamFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class TeamPagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = TeamPagerAdapter.class.getSimpleName();
    private Context mContext;
    private SelectTeamFragment selectTeamFragment;
    private ShowTeamFragment showTeamFragment;

    public TeamPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (selectTeamFragment == null) {
                selectTeamFragment = new SelectTeamFragment();
            }
            return selectTeamFragment;
        } else if (position == 1) {
            if (showTeamFragment == null) {
                showTeamFragment = new ShowTeamFragment();
            }
            return showTeamFragment;
        }
        return selectTeamFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return selectTeamFragment;
        } else if (selectedTabPosition == 1) {
            return showTeamFragment;
        }

        return selectTeamFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.select_team);
        } else if (position == 1) {
            return mContext.getString(R.string.show_team);
        }
        return mContext.getString(R.string.select_team);
    }

    @Override
    public int getCount() {
        return 2;
    }

}
