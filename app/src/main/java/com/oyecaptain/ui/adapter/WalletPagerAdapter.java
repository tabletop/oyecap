package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.BalanceFragment;
import com.oyecaptain.ui.fragment.BalanceHistoryFragment;
import com.oyecaptain.ui.fragment.LiveGamesFragment;
import com.oyecaptain.ui.fragment.PastGamesFragment;
import com.oyecaptain.ui.fragment.UpcomingGamesFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class WalletPagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = WalletPagerAdapter.class.getSimpleName();
    private Context mContext;
    private BalanceFragment balanceFragment;
    private BalanceHistoryFragment balanceHistoryFragment;

    public WalletPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (balanceFragment == null) {
                balanceFragment = new BalanceFragment();
            }
            return balanceFragment;
        } else if (position == 1) {
            if (balanceHistoryFragment == null) {
                balanceHistoryFragment = new BalanceHistoryFragment();
            }
            return balanceHistoryFragment;
        }
        return balanceFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return balanceFragment;
        } else if (selectedTabPosition == 1) {
            return balanceHistoryFragment;
        }

        return balanceFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.your_balance);
        } else if (position == 1) {
            return mContext.getString(R.string.your_history);
        }
        return mContext.getString(R.string.your_balance);
    }

    @Override
    public int getCount() {
        return 2;
    }

}
