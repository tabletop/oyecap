package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oyecaptain.R;
import com.oyecaptain.model.response.Transactions;
import com.oyecaptain.ui.fragment.BalanceHistoryFragment;

import java.util.List;

public class BalanceHistoryRecyclerAdapter extends RecyclerView.Adapter<BalanceHistoryRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Transactions> mlistData;
    private OnClickListener mClickListener;
    private int pos;

    public BalanceHistoryRecyclerAdapter(Context context, BalanceHistoryFragment balanceHistoryFragment) {
        mContext = context;
        mClickListener = (BalanceHistoryFragment) balanceHistoryFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Transactions> matches) {
        this.mlistData = matches;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_balance_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {

        viewholder.txtTransactionType.setText(getTransactionType(mlistData.get(position).getTransaction_type()));
        viewholder.txtTransaction.setText(mlistData.get(position).getTransaction_id() + "");
        viewholder.txtMatch.setText(" - " + mlistData.get(position).getLeague());


        //1-Debit 2-Credit
        if (mlistData.get(position).getDebit_credit() == 1) {
            viewholder.txtBalance.setText("-" + "\u20B9 " + (int) mlistData.get(position).getBalance());
            viewholder.txtBalance.setTextColor(mContext.getResources().getColor(R.color.red));
        } else {
            viewholder.txtBalance.setText("+" + "\u20B9 " + (int) mlistData.get(position).getBalance());
            viewholder.txtBalance.setTextColor(mContext.getResources().getColor(R.color.green));
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTransaction, txtMatch, txtTransactionType, txtBalance;

        public ViewHolder(View itemView) {
            super(itemView);

            txtTransaction = (TextView) itemView.findViewById(R.id.txtTransaction);
            txtMatch = (TextView) itemView.findViewById(R.id.txtMatch);
            txtTransactionType = (TextView) itemView.findViewById(R.id.txtTransactionType);
            txtBalance = (TextView) itemView.findViewById(R.id.txtBalance);
        }
    }

    private String getTransactionType(int type) {
        switch (type) {
            case 1:
                return "Admin";
            case 2:
                return "Refund";

            case 3:
                return "Winning";

            case 4:
                return "PayPal";
            case 5:
                return "Authorize";
            case 6:
                return "Withdrawal";
            case 7:
                return "Entry Fee";
            case 8:
                return "System";
            case 9:
                return "Admin";
            case 10:
                return "System";
            case 11:
                return "Citrus Pay";
            case 12:
                return "Citrus Pay";
            case 13:
                return "PayUMoney";
            case 14:
                return "PayU Money";
            case 15:
                return "Coupon";
        }
        return "";
    }


}
