package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.LiveGamesFragment;
import com.oyecaptain.ui.fragment.PastGamesFragment;
import com.oyecaptain.ui.fragment.UpcomingGamesFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class MyGamesAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = "MyGamesAdapter";
    private Context mContext;
    private LiveGamesFragment liveGamesFragment;
    private UpcomingGamesFragment upcomingGamesFragment;
    private PastGamesFragment pastGamesFragment;

    public MyGamesAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (liveGamesFragment == null) {
                liveGamesFragment = new LiveGamesFragment();
            }
            return liveGamesFragment;
        } else if (position == 1) {
            if (upcomingGamesFragment == null) {
                upcomingGamesFragment = new UpcomingGamesFragment();
            }
            return upcomingGamesFragment;
        } else if (position == 2) {
            if (pastGamesFragment == null) {
                pastGamesFragment = new PastGamesFragment();
            }
            return pastGamesFragment;
        }
        return liveGamesFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return liveGamesFragment;
        } else if (selectedTabPosition == 1) {
            return upcomingGamesFragment;
        } else if (selectedTabPosition == 2) {
            return pastGamesFragment;
        }
        return liveGamesFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.live);
        } else if (position == 1) {
            return mContext.getString(R.string.upcoming);
        } else if (position == 2) {
            return mContext.getString(R.string.past);
        }
        return mContext.getString(R.string.live);
    }

    @Override
    public int getCount() {
        return 3;
    }

}
