package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.AddCashFragment;
import com.oyecaptain.ui.fragment.MatchesFragment;
import com.oyecaptain.ui.fragment.MegaLeaguesFragment;
import com.oyecaptain.ui.fragment.MyContestFragment;
import com.oyecaptain.ui.fragment.SettingFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {

    final static String LOG_TAG = "HomePagerAdapter";

    private Context mContext;
    private MatchesFragment matchesFragment;
    private MegaLeaguesFragment megaLeaguesFragment;
    private MyContestFragment myContestFragment;
    private AddCashFragment addCashFragment;
    private SettingFragment settingFragment;

    private String title[] = {"MATCHES", "MEGALEAGUES", "MY GAMES", "ADD CASH", "SETTINGS"};
    private int titleIcon[] = {
            R.drawable.selector_matches,
            R.drawable.selector_megaleagues,
            R.drawable.selector_my_games,
            R.drawable.selector_add_cash,
            R.drawable.selector_settings
    };

    public HomePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (matchesFragment == null) {
                matchesFragment = new MatchesFragment();
            }
            return matchesFragment;
        } else if (position == 1) {
            if (megaLeaguesFragment == null) {
                megaLeaguesFragment = new MegaLeaguesFragment();
            }
            return megaLeaguesFragment;
        } else if (position == 2) {
            if (myContestFragment == null) {
                myContestFragment = new MyContestFragment();
            }
            return myContestFragment;
        } else if (position == 3) {
            if (addCashFragment == null) {
                addCashFragment = new AddCashFragment();
            }
            return addCashFragment;
        } else if (position == 4) {
            if (settingFragment == null) {
                settingFragment = new SettingFragment();
            }
            return settingFragment;
        }
        return megaLeaguesFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return matchesFragment;
        } else if (selectedTabPosition == 1) {
            return megaLeaguesFragment;
        } else if (selectedTabPosition == 2) {
            return myContestFragment;
        } else if (selectedTabPosition == 3) {
            return addCashFragment;
        } else if (selectedTabPosition == 4) {
            return settingFragment;
        }
        return matchesFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }


    public View getTabView(int position) {
        View v = null;
        v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.textView);
        tv.setText(title[position]);
        ImageView img = (ImageView) v.findViewById(R.id.imgView);
        img.setImageResource(titleIcon[position]);
        return v;
    }


    @Override
    public int getCount() {
        return 5;
    }

}
