package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oyecaptain.R;
import com.oyecaptain.ui.fragment.JoinedLeagueFragment;
import com.oyecaptain.ui.fragment.LiveGamesFragment;
import com.oyecaptain.ui.fragment.PastGamesFragment;
import com.oyecaptain.ui.fragment.PrivateLeagueFragment;
import com.oyecaptain.ui.fragment.PublicLeagueFragment;
import com.oyecaptain.ui.fragment.UpcomingGamesFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class LeagueSelectionPagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG_TAG = LeagueSelectionPagerAdapter.class.getSimpleName();
    private Context mContext;
    private PublicLeagueFragment publicLeagueFragment;
    private PrivateLeagueFragment privateLeagueFragment;
    private JoinedLeagueFragment joinedLeagueFragment;

    public LeagueSelectionPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (publicLeagueFragment == null) {
                publicLeagueFragment = new PublicLeagueFragment();
            }
            return publicLeagueFragment;
        } else if (position == 1) {
            if (privateLeagueFragment == null) {
                privateLeagueFragment = new PrivateLeagueFragment();
            }
            return privateLeagueFragment;
        } else if (position == 2) {
            if (joinedLeagueFragment == null) {
                joinedLeagueFragment = new JoinedLeagueFragment();
            }
            return joinedLeagueFragment;
        }
        return publicLeagueFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return publicLeagueFragment;
        } else if (selectedTabPosition == 1) {
            return privateLeagueFragment;
        } else if (selectedTabPosition == 2) {
            return joinedLeagueFragment;
        }
        return publicLeagueFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.public_league);
        } else if (position == 1) {
            return mContext.getString(R.string.private_league);
        } else if (position == 2) {
            return mContext.getString(R.string.joined_league);
        }
        return mContext.getString(R.string.public_league);
    }

    @Override
    public int getCount() {
        return 3;
    }

}
