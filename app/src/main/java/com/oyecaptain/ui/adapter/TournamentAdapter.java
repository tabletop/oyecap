package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oyecaptain.R;
import com.oyecaptain.model.response.Tournaments;
import com.oyecaptain.ui.fragment.MatchesFragment;

import java.util.List;

public class TournamentAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflator;
    private View.OnClickListener mOnClickListener;
    private List<Tournaments> mListdata;

    public TournamentAdapter(Context context, MatchesFragment matchesFragment) {
        mContext = context;
        mOnClickListener = matchesFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Tournaments> tournaments) {
        this.mListdata = tournaments;
    }


    @Override
    public int getCount() {
        return mListdata == null ? 0 : mListdata.size();
    }

    @Override
    public Object getItem(int position) {
        return mListdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        final ViewHolder viewholder;
        if (convertView == null) {
            viewholder = new ViewHolder();

            convertView = mInflator.inflate(R.layout.list_item_tournaments, null);

            viewholder.txtName = (TextView) convertView.findViewById(R.id.txtName);

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }

        viewholder.txtName.setText(mListdata.get(position).getName() + "");

        viewholder.txtName.setOnClickListener(mOnClickListener);

        viewholder.txtName.setTag(mListdata.get(position));

        /*if (mListdata.get(position).isSelected()) {
            viewholder.imgSelected.setVisibility(View.VISIBLE);
        } else {
            viewholder.imgSelected.setVisibility(View.GONE);
        }*/

        return convertView;
    }

    public static class ViewHolder {
        TextView txtName;
    }

}
