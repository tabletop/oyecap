package com.oyecaptain.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.krapps.ui.BaseActivity;
import com.krapps.utils.StringUtils;
import com.oyecaptain.R;
import com.oyecaptain.model.response.Matches;
import com.oyecaptain.model.response.Players;
import com.oyecaptain.ui.fragment.SelectTeamFragment;
import com.oyecaptain.ui.fragment.UpcomingGamesFragment;

import java.util.List;

public class PlayerRecyclerAdapter extends RecyclerView.Adapter<PlayerRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<Players> mlistData;
    private OnClickListener mClickListener;
    private int pos;
    private Matches mMatches;

    public PlayerRecyclerAdapter(Context context, SelectTeamFragment selectTeamFragment) {
        mContext = context;
        mClickListener = selectTeamFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<Players> players) {
        this.mlistData = players;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_player, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {

        viewholder.txtName.setText(mlistData.get(position).getName());
        viewholder.txtCreditPoint.setText(mlistData.get(position).getCredit() + "");
        viewholder.txtPoint.setText(mlistData.get(position).getPoints() + "");

        if (mMatches != null && mMatches.getTeams() != null && mMatches.getTeams().size() > 1) {
            if (mlistData.get(position).getTeam() == mMatches.getTeams().get(0).getId()) {
                viewholder.txtTeam.setText(mMatches.getTeams().get(0).getTeam_code() + "");
            } else {
                viewholder.txtTeam.setText(mMatches.getTeams().get(1).getTeam_code() + "");
            }
        }

        if (mlistData.get(position).isSelected()) {
            viewholder.imgSelected.setVisibility(View.VISIBLE);
            viewholder.rtlparent.setBackgroundResource(R.drawable.bg_green_border);
        } else {
            viewholder.imgSelected.setVisibility(View.GONE);
            viewholder.rtlparent.setBackgroundResource(android.R.color.transparent);
        }

        viewholder.rtlparent.setTag(mlistData.get(position));

        viewholder.rtlparent.setOnClickListener(mClickListener);
    }

    public void setMatches(Matches matches) {
        mMatches = matches;
    }

    public List<Players> getListData() {
        return mlistData;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtTeam, txtPoint;
        TextView txtCreditPoint;
        ImageView imgSelected;
        RelativeLayout rtlparent;

        public ViewHolder(View itemView) {
            super(itemView);

            rtlparent = (RelativeLayout) itemView.findViewById(R.id.rtlparent);
            imgSelected = (ImageView) itemView.findViewById(R.id.imgSelected);
            txtTeam = (TextView) itemView.findViewById(R.id.txtTeam);
            txtCreditPoint = (TextView) itemView.findViewById(R.id.txtCreditPoint);
            txtPoint = (TextView) itemView.findViewById(R.id.txtPoint);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
        }
    }


}
