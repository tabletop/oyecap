package com.oyecaptain.constants;

public interface AppConstants {

	public int	REQUEST_CODE_CAMERA						= 10001;

	public String EXTRA_BANNER                        	= "extra_shop";

	public String BUNDLE_USER							= "BUNDLE_USER";


	String OYE_CAP_PREFS 								= "OYE_CAP_PREFS";

	String EXTRA_TOURNAMENT_ID 							= "extra_tournament_id";
	String EXTRA_MATCH_ID 								= "extra_match_id";
	String EXTRA_MATCH	 								= "extra_match";
	String EXTRA_MATCH_NAME								= "extra_match_name";
	String EXTRA_AMOUNT									= "extra_amount";
	String EXTRA_COUPON_CODE							= "extra_coupon_code";
	String EXTRA_EMAIL									= "extra_email";
	String EXTRA_PLAYERS								= "extra_players";
	String EXTRA_SHOW_JOIN_MORE_LEAGUE					= "extra_show_join_more_league";
	String EXTRA_TEAM_EDITABLE							= "extra_team_editable";

}
