package com.oyecaptain.constants;

public interface ApiConstants {

	final int	REQUEST_REGISTER							= 1;
	final int	REQUEST_GET_USER_DETAILS					= 2;
	final int	REQUEST_UPDATE_USER_DETAILS					= 3;
	final int	REQUEST_LOGIN_FACEBOOK						= 4;
	final int	REQUEST_LOGIN_GOOGLE						= 5;
	final int	REQUEST_LOGIN_EMAIL							= 6;
	final int 	REQUEST_LOGOUT 								= 7;
	final int	REQUEST_VERIFY_EMAIL						= 8;
	final int	REQUEST_FORGOT_PASSWORD						= 9;
	final int	REQUEST_NEW_PASSWORD						= 10;
	final int	REQUEST_CHANGE_PASSWORD						= 11;
	final int	REQUEST_RESEND_REGISTER_MAIL				= 12;
	final int	REQUEST_GET_TOURNAMENTS						= 13;
	final int	REQUEST_GET_MATCHES							= 14;
	final int	REQUEST_JOIN_LEAGUE							= 15;
	final int	REQUEST_GET_MEGA_LEAGUES					= 16;
	final int	REQUEST_SAVE_TEAM							= 17;
	final int	REQUEST_GET_JOINED_LEAGUES					= 18;
	final int	REQUEST_GET_JOINED_MATCHES					= 19;
	final int	REQUEST_GET_PLAYERS							= 20;
	final int	REQUEST_GET_BALANCE							= 21;
	final int	REQUEST_GET_BALANCE_HISTORY					= 22;
	final int	REQUEST_CREATE_PRIVATE_LEAGUE				= 23;
	final int	REQUEST_INVITE_CODE							= 24;
	final int	REQUEST_GET_LEAGUES							= 25;
	final int	REQUEST_GENRETE_HASH       					= 26;
	final int	REQUEST_ADD_MONEY       					= 27;



	final String URL_WEB 							= "http://test.oyecaptain.com";
	final String URL_IMAGE 							= "http://admin.oyecaptain.com/assets/images/";
	final String URL_BASE							= "http://ec2-52-221-233-176.ap-southeast-1.compute.amazonaws.com:7700";//prod
	final String URL_LOGIN							= URL_BASE + "/login";
	final String URL_LOGOUT							= URL_BASE + "/logout";
	final String URL_VERIFY_EMAIL					= URL_BASE + "/verify";
	final String URL_FORGOT_PASSWORD				= URL_BASE + "/forgot_password";
	final String URL_NEW_PASSWORD					= URL_BASE + "/new_password";
	final String URL_CHANGE_PASSWORD				= URL_BASE + "/change_password";
	final String URL_RESEND_REGISTER_MAIL			= URL_BASE + "/resend_register_mail";
	final String URL_REGISTER						= URL_BASE + "/register";
	final String URL_USER_DETAILS					= URL_BASE + "/user_details";
	final String URL_TOURNAMENTS					= URL_BASE + "/tournaments";
	final String URL_MATCHES						= URL_BASE + "/matches?tournament_id=%s";
	final String URL_JOIN_LEAGUE					= URL_BASE + "/join_league?league_id=%s&match_id=%s";
	final String URL_MEGA_LEAGUES					= URL_BASE + "/mega_leagues";
	final String URL_SAVE_TEAM						= URL_BASE + "/save_team";
	final String URL_LEAGUES						= URL_BASE + "/leagues?match_id=%s";
	final String URL_JOINED_LEAGUES					= URL_BASE + "/joined_leagues?match_id=%s";
	final String URL_JOINED_MATCHES					= URL_BASE + "/joined_matches";
	final String URL_PLAYERS						= URL_BASE + "/players?match_id=%s&tournament_id=%s";
	final String URL_BALANCE						= URL_BASE + "/balance";
	final String URL_BALANCE_HISTORY				= URL_BASE + "/transaction_history";
	final String URL_CREATE_PRIVATE_LEAGUE			= URL_BASE + "/private_league";
	final String URL_INVITE_CODE					= URL_BASE + "/invite_code?league_id=%s";
	final String URL_ADD_MONEY						= URL_BASE + "/add_money";
	final String URL_GENERATE_HASH					= URL_BASE + "/generate_hash";
	final String URL_GENERATE_CHECKSUM				= URL_BASE + "/generate_checksum";
	final String URL_VERIFY_CHECKSUM				= URL_BASE + "/verify_checksum";

}
