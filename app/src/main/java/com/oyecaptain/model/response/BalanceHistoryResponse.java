package com.oyecaptain.model.response;

import java.util.List;

/**
 * Created by K.Agg on 11-11-2016.
 */

public class BalanceHistoryResponse {

    private List<Transactions> transactions;

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }
}
