package com.oyecaptain.model.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by monish on 08/11/16.
 */

public class Teams implements Parcelable {
    private int id;
    private String name;
    private String img_url;
    private String team_code;


    public Teams(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.img_url = in.readString();
        this.team_code = in.readString();
    }

    public Teams() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(img_url);
        dest.writeString(team_code);

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getTeam_code() {
        return team_code;
    }

    public void setTeam_code(String team_code) {
        this.team_code = team_code;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Teams> CREATOR = new Parcelable.Creator<Teams>() {

        public Teams createFromParcel(Parcel in) {
            return new Teams(in);
        }

        public Teams[] newArray(int size) {
            return new Teams[size];
        }
    };

}
