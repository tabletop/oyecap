package com.oyecaptain.model.response;

/**
 * Created by K.Agg on 11-11-2016.
 */

public class BalanceResponse {
    Balance balance;

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }
}
