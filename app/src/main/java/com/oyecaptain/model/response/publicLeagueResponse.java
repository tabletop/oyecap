package com.oyecaptain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by monish on 12/11/16.
 */

public class PublicLeagueResponse extends CommonJsonResponse {
    private Matches match;

    public Matches getMatch() {
        return match;
    }

    public void setMatch(Matches match) {
        this.match = match;
    }
}
