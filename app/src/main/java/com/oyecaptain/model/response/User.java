package com.oyecaptain.model.response;

/**
 * Created by monish on 12/11/16.
 */

public class User {
    private int id;
    private String user_name;
    private String screen_name;
    private String user_email;
    private String user_dob;
    private String user_image;
    private String user_registration;
    private String fb_status;
    private String invited_by;
    private String invite_code;
    private String google_status;
    private String phone_num;
    private String country_code;
    private int first_time;
    private int ban;
    private String ban_reason;
    private String refcode;
    private String referrer;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_dob() {
        return user_dob;
    }

    public void setUser_dob(String user_dob) {
        this.user_dob = user_dob;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_registration() {
        return user_registration;
    }

    public void setUser_registration(String user_registration) {
        this.user_registration = user_registration;
    }

    public String getFb_status() {
        return fb_status;
    }

    public void setFb_status(String fb_status) {
        this.fb_status = fb_status;
    }

    public String getInvited_by() {
        return invited_by;
    }

    public void setInvited_by(String invited_by) {
        this.invited_by = invited_by;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }

    public String getGoogle_status() {
        return google_status;
    }

    public void setGoogle_status(String google_status) {
        this.google_status = google_status;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public int getFirst_time() {
        return first_time;
    }

    public void setFirst_time(int first_time) {
        this.first_time = first_time;
    }

    public int getBan() {
        return ban;
    }

    public void setBan(int ban) {
        this.ban = ban;
    }

    public String getBan_reason() {
        return ban_reason;
    }

    public void setBan_reason(String ban_reason) {
        this.ban_reason = ban_reason;
    }

    public String getRefcode() {
        return refcode;
    }

    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }
}
