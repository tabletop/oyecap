package com.oyecaptain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by monish on 12/11/16.
 */

public class JoinedLeagueResponse extends CommonJsonResponse {
    @SerializedName("public_leagues")
    @Expose
    private List<PublicLeagues> publicLeagues;

    @SerializedName("private_leagues")
    @Expose
    private List<PublicLeagues> privateLeagues;

    public List<PublicLeagues> getPublicLeagues() {
        return publicLeagues;
    }

    public void setPublicLeagues(List<PublicLeagues> publicLeagues) {
        this.publicLeagues = publicLeagues;
    }

    public List<PublicLeagues> getPrivateLeagues() {
        return privateLeagues;
    }

    public void setPrivateLeagues(List<PublicLeagues> privateLeagues) {
        this.privateLeagues = privateLeagues;
    }
}
