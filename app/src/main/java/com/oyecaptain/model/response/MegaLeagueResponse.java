package com.oyecaptain.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by monish on 08/11/16.
 */

public class MegaLeagueResponse extends CommonJsonResponse {
    @SerializedName("mega_leagues")
    @Expose
    private List<MegaLeagues> megaLeagues;

    public List<MegaLeagues> getMegaLeagues() {
        return megaLeagues;
    }

    public void setMegaLeagues(List<MegaLeagues> megaLeagues) {
        this.megaLeagues = megaLeagues;
    }
}
