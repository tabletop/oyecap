package com.oyecaptain.model.response;

/**
 * Created by K.Agg on 13-11-2016.
 */

public class HashResponse {

    private String hash;
    private String string;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
