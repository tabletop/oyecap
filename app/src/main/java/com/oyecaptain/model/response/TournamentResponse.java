package com.oyecaptain.model.response;

import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by monish on 08/11/16.
 */

public class TournamentResponse extends CommonJsonResponse {
    private List<Tournaments> tournaments;

    public List<Tournaments> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournaments> tournaments) {
        this.tournaments = tournaments;
    }
}
