package com.oyecaptain.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 08/11/16.
 */

public class LoginResponse extends CommonJsonResponse {

    private String user_id;
    private String token;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
