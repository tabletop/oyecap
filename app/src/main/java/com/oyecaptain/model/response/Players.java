package com.oyecaptain.model.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by monish on 13/11/16.
 */

public class Players implements Parcelable {

    private int id;
    private int type;
    private int team;
    private int points;
    private String name;
    private String p_type;
    private float credit;
    private boolean selected;
    private boolean isViceCaptain;
    private boolean isCaptain;


    public Players(Parcel in) {
        this.id = in.readInt();
        this.type = in.readInt();
        this.team = in.readInt();
        this.points = in.readInt();
        this.name = in.readString();
        this.p_type = in.readString();
        this.credit = in.readFloat();
        this.selected = in.readByte() == 1 ? true : false;
        this.isViceCaptain = in.readByte() == 1 ? true : false;
        this.isCaptain = in.readByte() == 1 ? true : false;
    }

    public Players() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(type);
        dest.writeInt(team);
        dest.writeInt(points);
        dest.writeString(name);
        dest.writeString(p_type);
        dest.writeFloat(credit);
        dest.writeByte((byte) (selected ? 1 : 0));
        dest.writeByte((byte) (isViceCaptain ? 1 : 0));
        dest.writeByte((byte) (isCaptain ? 1 : 0));
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }


    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getP_type() {
        return p_type;
    }

    public void setP_type(String p_type) {
        this.p_type = p_type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isViceCaptain() {
        return isViceCaptain;
    }

    public void setViceCaptain(boolean viceCaptain) {
        isViceCaptain = viceCaptain;
    }

    public boolean isCaptain() {
        return isCaptain;
    }

    public void setCaptain(boolean captain) {
        isCaptain = captain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Players players = (Players) o;

        return id == players.id;

    }

    @Override
    public int hashCode() {
        return id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Players> CREATOR = new Parcelable.Creator<Players>() {

        public Players createFromParcel(Parcel in) {
            return new Players(in);
        }

        public Players[] newArray(int size) {
            return new Players[size];
        }
    };
}
