package com.oyecaptain.model.response;

import java.util.List;

/**
 * Created by monish on 13/11/16.
 */

public class PlayersResponse {
    private boolean selected;
    private List<Players> players;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<Players> getPlayers() {
        return players;
    }

    public void setPlayers(List<Players> players) {
        this.players = players;
    }
}
