package com.oyecaptain.model.response;

/**
 * Created by K.Agg on 11-11-2016.
 */

public class Balance {

    private float balance;
    private float unutilized;
    private float winning;
    private float bonus;

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getUnutilized() {
        return unutilized;
    }

    public void setUnutilized(float unutilized) {
        this.unutilized = unutilized;
    }

    public float getWinning() {
        return winning;
    }

    public void setWinning(float winning) {
        this.winning = winning;
    }

    public float getBonus() {
        return bonus;
    }

    public void setBonus(float bonus) {
        this.bonus = bonus;
    }
}
