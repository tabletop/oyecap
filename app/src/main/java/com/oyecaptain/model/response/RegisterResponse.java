package com.oyecaptain.model.response;

/**
 * Created by K.Agg on 11-11-2016.
 */

public class RegisterResponse {

    private String user_id;
    private String token;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
