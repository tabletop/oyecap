package com.oyecaptain.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 08/11/16.
 */

public class UserDetailResponse extends CommonJsonResponse {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
