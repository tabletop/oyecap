package com.oyecaptain.model.response;

/**
 * Created by monish on 12/11/16.
 */

public class InviteCodeResponse {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
