package com.oyecaptain.model.response;

import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by monish on 08/11/16.
 */

public class MatchesResponse extends CommonJsonResponse {
    private List<Matches> matches;

    public List<Matches> getMatches() {
        return matches;
    }

    public void setMatches(List<Matches> matches) {
        this.matches = matches;
    }
}
