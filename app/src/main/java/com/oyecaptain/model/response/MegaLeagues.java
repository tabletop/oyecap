package com.oyecaptain.model.response;

import java.util.Date;
import java.util.List;

/**
 * Created by monish on 08/11/16.
 */

public class MegaLeagues {
    private int id;
    private int win;
    private int fee;
    private int total_seats;
    private int filled_seats;
    private int tournament_id;
    private String tournament_name;
    private String time;
    private String format_time;
    private String mega;
    private boolean joined;
    private int match_id;
    private boolean team_sel;
    private long time_diff;
    private String match_name;
    private List<Teams> teams;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public int getTotal_seats() {
        return total_seats;
    }

    public void setTotal_seats(int total_seats) {
        this.total_seats = total_seats;
    }

    public int getFilled_seats() {
        return filled_seats;
    }

    public void setFilled_seats(int filled_seats) {
        this.filled_seats = filled_seats;
    }

    public String getTournament_name() {
        return tournament_name;
    }

    public void setTournament_name(String tournament_name) {
        this.tournament_name = tournament_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getFormat_time() {
        return format_time;
    }

    public void setFormat_time(String format_time) {
        this.format_time = format_time;
    }

    public String getMega() {
        return mega;
    }

    public void setMega(String mega) {
        this.mega = mega;
    }


    public boolean isJoined() {
        return joined;
    }

    public void setJoined(boolean joined) {
        this.joined = joined;
    }

    public boolean isTeam_sel() {
        return team_sel;
    }

    public void setTeam_sel(boolean team_sel) {
        this.team_sel = team_sel;
    }

    public long getTime_diff() {
        return time_diff;
    }

    public void setTime_diff(long time_diff) {
        this.time_diff = time_diff;
    }

    public String getMatch_name() {
        return match_name;
    }

    public void setMatch_name(String match_name) {
        this.match_name = match_name;
    }

    public List<Teams> getTeams() {
        return teams;
    }

    public void setTeams(List<Teams> teams) {
        this.teams = teams;
    }

    public int getTournament_id() {
        return tournament_id;
    }

    public void setTournament_id(int tournament_id) {
        this.tournament_id = tournament_id;
    }

    public int getMatch_id() {
        return match_id;
    }

    public void setMatch_id(int match_id) {
        this.match_id = match_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MegaLeagues that = (MegaLeagues) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
