package com.oyecaptain.model.response;

import java.util.List;

/**
 * Created by monish on 17/11/16.
 */

public class LeaguesCategories {
    private float price;
    private List<PublicLeagues> leagues;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<PublicLeagues> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<PublicLeagues> leagues) {
        this.leagues = leagues;
    }
}
