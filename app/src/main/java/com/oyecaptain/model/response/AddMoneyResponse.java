package com.oyecaptain.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 12/11/16.
 */

public class AddMoneyResponse extends CommonJsonResponse{
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
