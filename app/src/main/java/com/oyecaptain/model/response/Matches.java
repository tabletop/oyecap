package com.oyecaptain.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monish on 08/11/16.
 */

public class Matches implements Parcelable {
    private int id;
    private Long time = 0L;
    private String format_time;
    private int tournament;
    private String name;
    private int win;
    private int status;
    private float total_score;
    private float won;
    private boolean joined;
    private List<Teams> teams;
    @SerializedName("mega_leagues")
    @Expose
    private List<PublicLeagues> megaLeagues;

    @SerializedName("leagues_categories")
    @Expose
    private List<LeaguesCategories> leaguesCategoriesList;


    public Matches(Parcel in) {
        this.id = in.readInt();
        this.time = in.readLong();
        this.format_time = in.readString();
        this.tournament = in.readInt();
        this.name = in.readString();
        this.win = in.readInt();
        this.status = in.readInt();
        this.total_score = in.readFloat();
        this.won = in.readFloat();
        this.teams = new ArrayList<Teams>();
        in.readTypedList(teams, Teams.CREATOR);
    }

    public Matches() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeLong(time);
        dest.writeString(format_time);
        dest.writeInt(tournament);
        dest.writeString(name);
        dest.writeInt(win);
        dest.writeInt(status);
        dest.writeFloat(total_score);
        dest.writeFloat(won);
        dest.writeTypedList(teams);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getFormat_time() {
        return format_time;
    }

    public void setFormat_time(String format_time) {
        this.format_time = format_time;
    }

    public int getTournament() {
        return tournament;
    }

    public void setTournament(int tournament) {
        this.tournament = tournament;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public List<Teams> getTeams() {
        return teams;
    }

    public void setTeams(List<Teams> teams) {
        this.teams = teams;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public float getTotal_score() {
        return total_score;
    }

    public void setTotal_score(float total_score) {
        this.total_score = total_score;
    }


    public float getWon() {
        return won;
    }

    public void setWon(float won) {
        this.won = won;
    }

    public boolean isJoined() {
        return joined;
    }

    public void setJoined(boolean joined) {
        this.joined = joined;
    }


    public List<PublicLeagues> getMegaLeagues() {
        return megaLeagues;
    }

    public void setMegaLeagues(List<PublicLeagues> megaLeagues) {
        this.megaLeagues = megaLeagues;
    }

    public List<LeaguesCategories> getLeaguesCategoriesList() {
        return leaguesCategoriesList;
    }

    public void setLeaguesCategoriesList(List<LeaguesCategories> leaguesCategoriesList) {
        this.leaguesCategoriesList = leaguesCategoriesList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Matches> CREATOR = new Parcelable.Creator<Matches>() {

        public Matches createFromParcel(Parcel in) {
            return new Matches(in);
        }

        public Matches[] newArray(int size) {
            return new Matches[size];
        }
    };
}
