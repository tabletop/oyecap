package com.oyecaptain.model.request;

/**
 * Created by monish on 15/11/16.
 */

public class JoinLeagueRequest {
    private String league_id;

    public String getLeague_id() {
        return league_id;
    }

    public void setLeague_id(String league_id) {
        this.league_id = league_id;
    }
}
