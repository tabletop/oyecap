package com.oyecaptain.model.request;

/**
 * Created by K.Agg on 15-11-2016.
 */

public class GoogleLoginRequest {

    private String google_access_token;

    public String getGoogle_access_token() {
        return google_access_token;
    }

    public void setGoogle_access_token(String google_access_token) {
        this.google_access_token = google_access_token;
    }
}
