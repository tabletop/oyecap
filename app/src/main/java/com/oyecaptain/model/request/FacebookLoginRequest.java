package com.oyecaptain.model.request;

/**
 * Created by monish on 08/11/16.
 */

public class FacebookLoginRequest {
    private String facebook_access_token;

    public String getFacebook_access_token() {
        return facebook_access_token;
    }

    public void setFacebook_access_token(String facebook_access_token) {
        this.facebook_access_token = facebook_access_token;
    }
}
