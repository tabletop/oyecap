package com.oyecaptain.model.request;

/**
 * Created by K.Agg on 12-11-2016.
 */

public class ForgotPasswordRequest {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
