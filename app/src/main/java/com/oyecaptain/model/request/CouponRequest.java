package com.oyecaptain.model.request;

/**
 * Created by monish on 12/11/16.
 */

public class CouponRequest {
    private String medium;
    private String coupon;

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
}
