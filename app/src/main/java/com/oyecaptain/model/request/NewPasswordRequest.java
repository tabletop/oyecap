package com.oyecaptain.model.request;

/**
 * Created by K.Agg on 12-11-2016.
 */

public class NewPasswordRequest {

    private String email;
    private String otp;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
