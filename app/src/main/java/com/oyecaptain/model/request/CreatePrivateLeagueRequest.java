package com.oyecaptain.model.request;

/**
 * Created by monish on 12/11/16.
 */

public class CreatePrivateLeagueRequest {
    private int fixture_id;
    private String league;
    private String fee;
    private String prize;
    private int league_size;
    private long live_in;


    public int getFixture_id() {
        return fixture_id;
    }

    public void setFixture_id(int fixture_id) {
        this.fixture_id = fixture_id;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public int getLeague_size() {
        return league_size;
    }

    public void setLeague_size(int league_size) {
        this.league_size = league_size;
    }

    public long getLive_in() {
        return live_in;
    }

    public void setLive_in(long live_in) {
        this.live_in = live_in;
    }
}
